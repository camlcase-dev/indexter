## 2021-03-10 -- v1.1.0
* Added FA2 support in Indexter Go
* Added additional helpers in Makefile
* Added local and kubernetes deployments
* Added sanity checks in critical parts of code
* Cleaned up server and middleware
* Added testing around the FA2 cache
* Added .gitignore

## 2021-03-10 -- v1.0.0
* Begining of changelog. 
* First major version tag.