PROJECT_NAME := "indexter"
VERSION := "1.2.1"
PKG := "gitlab.com/camlcase-dev/$(PROJECT_NAME)"
PKG_LIST := $(shell go list ${PKG}/... | grep -v /vendor/)
GO_FILES := $(shell find . -name '*.go' | grep -v /vendor/ | grep -v _test.go)

.PHONY: dep clean test coverage coverhtml lint

checks: fmt lint staticcheck race  ## Runs all quality checks

lint: ## Lint the files
	@golint -set_exit_status ${PKG_LIST}

staticcheck: ## Static check the files
	@staticcheck ${PKG_LIST}

fmt: ## Static check the files
	@go fmt ${PKG_LIST} 

test: ## Run unittests
	@go test -v ${PKG_LIST}

race: ## Run data race detector
	@go test -race -v ${PKG_LIST}

dep: ## Get the dependencies
	@go get -u golang.org/x/lint/golint
	@go get -u honnef.co/go/tools/...

clean: ## Remove previous build
	@rm -f $(PROJECT_NAME)

version: ## Remove previous build
	@echo $(VERSION)

build:
	@docker build -t camlcasetezos/indexter:$(VERSION) .
	@docker build -t camlcasetezos/indexter-fa2:$(VERSION) internal-fa2-service/. -f internal-fa2-service/Dockerfile
	@docker push camlcasetezos/indexter:$(VERSION) 
	@docker push camlcasetezos/indexter-fa2:$(VERSION)

build-dev:
	@docker build -t camlcasetezos/indexter:$(VERSION)-dev .
	@docker build -t camlcasetezos/indexter-fa2:$(VERSION)-dev internal-fa2-service/. -f internal-fa2-service/Dockerfile
	@docker push camlcasetezos/indexter:$(VERSION)-dev 
	@docker push camlcasetezos/indexter-fa2:$(VERSION)-dev

tag:
	@git tag -a $(VERSION) -m "New release $(VERSION)"
	@git push --tags

tag-dev:
	@git tag -a $(VERSION)-dev -m "New dev release $(VERSION)-dev"
	@git push --tags

help: ## Display this help screen
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'