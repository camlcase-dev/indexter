# Indexter

Indexter (v1.1.0) is an FA1.2 and FA2 API to query for balances, supply, permissions, allowances, and tokens. 

## Endpoints

| PATH                                         | Description                                          | type        | error    |
|----------------------------------------------|------------------------------------------------------|:-----------:|:--------:|
| /fa12/{contract}/balance/{owner}             | Gets the balance of an {owner} in an FA1.2 {contract}| string (int)| object   |
| /fa12/{contract}/balance/{owner}/now         | Gets the balance of an {owner} in an FA1.2 {contract} without the cache| string (int)| object   |
| /fa12/{contract}/supply                      | Gets the supply of an FA1.2 {contract}               | string (int)| object   |
| /fa12/{contract}/supply/now                  | Gets the supply of an FA1.2 {contract}  without the cache| string (int)| object   |
| /fa12/{contract}/allowance/{owner}/{spender} | Gets the allowance of a {spender} for an {owner} in an FA1.2 {contract}  | string (int)| object   |
| /fa2/{contract}/{tokenid}/balance/{owner}     | Gets the balance of an {owner} for an FA2 {contract} and {tokenid}| string (int)| object   |
| /fa2/{contract}/{tokenid}/balance/{owner}/now | Gets the balance of an {owner} for an FA2 {contract} and {tokenid} without the cache| string (int)| object   |
| /fa2/{contract}/{tokenID}/supply                      | Gets the supply of an FA2 {contract}               | string (int)| object   |
| /fa2/{contract}/{tokenID}/supply/now                  | Gets the supply of an FA2 {contract}  without the cache| string (int)| object   |
| /fa2/{contract}/tokens                      | Gets the tokens of an FA2 {contract}               | []string (int)| object   |
| /fa2/{contract}/tokens/now                  | Gets the tokens of an FA2 {contract}  without the cache| []string (int)| object   |
| /fa2/{contract}/{tokenID}/permission/{owner}/{operator} | Get permissions status for an {operator} of a give {owner} for an FA2 {contract} and {tokenid}| bool | object   |
| /fa2/{contract}/{tokenID}/permission/{owner}/{operator}/now | Get permissions status for an {operator} of a give {owner} for an FA2 {contract} and {tokenid}| bool | object   |
| /currencies                                  | Gets the list of currencies with decimals and images | Object | object   |


## Running Locally
`docker-compose up --build`

## Test Coverage
```
➜  fa12cache git:(master) go test -v --cover
=== RUN   Test_GetAllowance
=== RUN   Test_GetAllowance/handles_failure_to_get_head
=== RUN   Test_GetAllowance/handles_failure_to_get_fa1.2_allowance
=== RUN   Test_GetAllowance/is_successful
--- PASS: Test_GetAllowance (0.00s)
    --- PASS: Test_GetAllowance/handles_failure_to_get_head (0.00s)
    --- PASS: Test_GetAllowance/handles_failure_to_get_fa1.2_allowance (0.00s)
    --- PASS: Test_GetAllowance/is_successful (0.00s)
=== RUN   Test_GetBalance
=== RUN   Test_GetBalance/handles_failure_to_make_new_cache
=== RUN   Test_GetBalance/handles_failure_to_get_balance
=== RUN   Test_GetBalance/is_successful_with_new_cache
=== RUN   Test_GetBalance/is_successful_with_existing_cache
--- PASS: Test_GetBalance (0.00s)
    --- PASS: Test_GetBalance/handles_failure_to_make_new_cache (0.00s)
    --- PASS: Test_GetBalance/handles_failure_to_get_balance (0.00s)
    --- PASS: Test_GetBalance/is_successful_with_new_cache (0.00s)
    --- PASS: Test_GetBalance/is_successful_with_existing_cache (0.00s)
=== RUN   Test_startBalanceWorker
=== RUN   Test_startBalanceWorker/handles_failure_to_update_balance
=== RUN   Test_startBalanceWorker/is_successful
--- PASS: Test_startBalanceWorker (1.01s)
    --- PASS: Test_startBalanceWorker/handles_failure_to_update_balance (0.50s)
    --- PASS: Test_startBalanceWorker/is_successful (0.50s)
=== RUN   Test_getBalance
=== RUN   Test_getBalance/handles_failure_to_get_head
=== RUN   Test_getBalance/handles_failure_to_get_fa1.2_balance
=== RUN   Test_getBalance/is_successful
--- PASS: Test_getBalance (0.00s)
    --- PASS: Test_getBalance/handles_failure_to_get_head (0.00s)
    --- PASS: Test_getBalance/handles_failure_to_get_fa1.2_balance (0.00s)
    --- PASS: Test_getBalance/is_successful (0.00s)
=== RUN   Test_updateBalance
=== RUN   Test_updateBalance/handles_failure_to_get_balance
=== RUN   Test_updateBalance/is_successful
--- PASS: Test_updateBalance (0.00s)
    --- PASS: Test_updateBalance/handles_failure_to_get_balance (0.00s)
    --- PASS: Test_updateBalance/is_successful (0.00s)
=== RUN   Test_GetSupply
=== RUN   Test_GetSupply/failes_to_make_new_cache
=== RUN   Test_GetSupply/is_successful_with_new_cache
=== RUN   Test_GetSupply/is_successful_with_existing_cache
--- PASS: Test_GetSupply (0.00s)
    --- PASS: Test_GetSupply/failes_to_make_new_cache (0.00s)
    --- PASS: Test_GetSupply/is_successful_with_new_cache (0.00s)
    --- PASS: Test_GetSupply/is_successful_with_existing_cache (0.00s)
=== RUN   Test_startSupplyWorker
=== RUN   Test_startSupplyWorker/handles_failure_to_load_contract
=== RUN   Test_startSupplyWorker/handles_failure_to_updateSupply
=== RUN   Test_startSupplyWorker/is_successful
--- PASS: Test_startSupplyWorker (1.50s)
    --- PASS: Test_startSupplyWorker/handles_failure_to_load_contract (0.50s)
    --- PASS: Test_startSupplyWorker/handles_failure_to_updateSupply (0.50s)
    --- PASS: Test_startSupplyWorker/is_successful (0.50s)
=== RUN   Test_getSupply
=== RUN   Test_getSupply/handles_failure_to_get_head
=== RUN   Test_getSupply/handles_failure_to_get_fa1.2_supply
=== RUN   Test_getSupply/is_successful
--- PASS: Test_getSupply (0.00s)
    --- PASS: Test_getSupply/handles_failure_to_get_head (0.00s)
    --- PASS: Test_getSupply/handles_failure_to_get_fa1.2_supply (0.00s)
    --- PASS: Test_getSupply/is_successful (0.00s)
=== RUN   Test_updateSupply
=== RUN   Test_updateSupply/handles_failure_to_get_supply
=== RUN   Test_updateSupply/is_successful
--- PASS: Test_updateSupply (0.00s)
    --- PASS: Test_updateSupply/handles_failure_to_get_supply (0.00s)
    --- PASS: Test_updateSupply/is_successful (0.00s)
PASS
coverage: 99.2% of statements
ok      gitlab.com/camlcase-dev/indexter/internal/fa12cache     2.680s
```
