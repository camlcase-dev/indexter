module gitlab.com/camlcase-dev/indexter

go 1.14

require (
	github.com/go-playground/validator/v10 v10.4.1
	github.com/go-resty/resty/v2 v2.5.0
	github.com/goat-systems/go-tezos/v4 v4.0.4
	github.com/google/uuid v1.2.0
	github.com/gorilla/mux v1.8.0
	github.com/patrickmn/go-cache v2.1.0+incompatible
	github.com/pkg/errors v0.9.1
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/testify v1.7.0
	gopkg.in/yaml.v2 v2.4.0
)
