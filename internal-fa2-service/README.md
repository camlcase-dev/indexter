# internal-fa2-service

This reads `../indexter.yml` for tezos node URLs. 
It expects an environment variable `INTERNAL_FA2_SERVICE_PORT` for the port number.
It requires yarn.

```
yarn
yarn start
```
