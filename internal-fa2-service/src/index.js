import { TezosToolkit } from '@taquito/taquito';
import { Tzip16Module, tzip16 } from '@taquito/tzip16';
import express from 'express';
import fs from 'fs';
import yaml from 'js-yaml';
// yarn start --config '../indexter.yml'

const args = process.argv.slice(2);
console.log(args);
var config_file_path = '';

switch (args[0]) {
case '--config':
  config_file_path = args[1]
  break;
default:
  console.log('Expected a config file');
  process.exit(1);
}


try {
  const config = yaml.load(fs.readFileSync(config_file_path, 'utf8'));
  var tezosClients = {};
  for (let network of config['networks']) {
    const tezosClient = new TezosToolkit(network['rpc_address']);
    tezosClient.addExtension(new Tzip16Module());
    tezosClients[network['name']] = tezosClient;
  };
} catch (e) {
  console.log('Did not find ', config_file_path, ':', e);
}

const app = express();
const port = process.env.INTERNAL_FA2_SERVICE_PORT || 3000;

// permission status of an operator for an owner of an FA2 contract and token id
app.get('/fa2/:contract/:token_id/permission/:owner/:operator', (req, res) => {
  const network =  req.query.network || 'mainnet';
  tezosClients[network].contract
    .at(req.params.contract, tzip16)
    .then((contract) => {
      return contract.tzip16().metadataViews();
    })     
    .then((views) => {
      return views['is_operator']().executeView(req.params.owner, req.params.operator, req.params.token_id)
    })
    .then((result) => {
      res.send(result)
    })
    .catch((error) => {
      console.log(`/fa2/:contract/:token_id/permission/:owner/:operator error: ${error} ${JSON.stringify(error, null, 2)}`)
      res.status(404).send(JSON.stringify(error))
    });
});

// balance for an owner of an FA2 contract and token id
app.get('/fa2/:contract/:token_id/balance/:owner', (req, res) => {
  const network =  req.query.network || 'mainnet';
  console.log(network);
  tezosClients[network].contract
    .at(req.params.contract, tzip16)
    .then((contract) => {
      return contract.tzip16().metadataViews();
    })     
    .then((views) => {
      return views['get_balance']().executeView(req.params.owner, req.params.token_id)
    })
    .then((result) => {
      res.send(result)
    })
    .catch((error) => {
      console.log(`/fa2/:contract/:token_id/balance/:owner error: ${error} ${JSON.stringify(error, null, 2)}`)
      res.status(404).send(JSON.stringify(error))
    });
});

// list the total supply of an FA2 contract and token id
app.get('/fa2/:contract/:token_id/total/supply', (req, res) => {
  const network =  req.query.network || 'mainnet';
  tezosClients[network].contract
    .at(req.params.contract, tzip16)
    .then((contract) => {
      return contract.tzip16().metadataViews();
    })     
    .then((views) => {
      return views['total_supply']().executeView(req.params.token_id)
    })
    .then((result) => {
      res.send(result)
    })
    .catch((error) => {
      console.log(`/fa2/:contract/:token_id/total/supply error: ${error} ${JSON.stringify(error, null, 2)}`)
      res.status(404).send(JSON.stringify(error))
    });
});

// list all of the tokens available for a given fa2 contract
app.get('/fa2/:contract/tokens', (req, res) => {
  const network =  req.query.network || 'mainnet';
  tezosClients[network].contract
    .at(req.params.contract, tzip16)
    .then((contract) => {
      return contract.tzip16().metadataViews();
    })     
    .then((views) => {
      return views['all_tokens']().executeView()
    })
    .then((result) => {
      res.send(result)
    })
    .catch((error) => {
      console.log(`/fa2/:contract/tokens error: ${error} ${JSON.stringify(error, null, 2)}`)
      res.status(404).send(JSON.stringify(error))
    });
});

app.listen(port, () => {
  console.log(`internal-fa2-service app listening at http://localhost:${port}`)
})

// example queries
// http://localhost:3000/fa2/KT1REEb5VxWRjcHm5GzDMwErMmNFftsE5Gpf/tokens
// http://localhost:3000/fa2/KT1REEb5VxWRjcHm5GzDMwErMmNFftsE5Gpf/0/total/supply
// http://localhost:3000/fa2/KT1REEb5VxWRjcHm5GzDMwErMmNFftsE5Gpf/0/permission/tz1i2tE6hic2ASe9Kvy85ar5hGSSc58bYejT/tz1i2tE6hic2ASe9Kvy85ar5hGSSc58bYejT
// http://localhost:3000/fa2/KT1REEb5VxWRjcHm5GzDMwErMmNFftsE5Gpf/0/balance/tz1i2tE6hic2ASe9Kvy85ar5hGSSc58bYejT
