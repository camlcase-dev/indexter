package allowance

import (
	"context"
	"errors"
	"fmt"
	"time"

	"github.com/google/uuid"
	gocache "github.com/patrickmn/go-cache"
	"github.com/sirupsen/logrus"
	"gitlab.com/camlcase-dev/indexter/internal/cache"
	contextx "gitlab.com/camlcase-dev/indexter/internal/context"
)

// Cache is a cache and worker for FA1.2 allowancess
type Cache interface {
	Get(ctx context.Context, strs ...string) string
	Set(allowance string, strs ...string)
}

type cachex struct {
	expiration     time.Duration
	purgeFrequency time.Duration
	updateInterval time.Duration
	updateFunc     func(ctx context.Context, params ...string) (string, error)
	logger         *logrus.Logger
	cache          *gocache.Cache
}

type allowanceItem struct {
	lastQueried time.Time
	allowed     string
}

// New returns a new allowance Cache
func New(logger *logrus.Logger, expiration time.Duration, purgeFrequency time.Duration, updateInterval time.Duration, updateFunc func(ctx context.Context, params ...string) (string, error)) Cache {
	c := &cachex{
		expiration:     expiration,
		purgeFrequency: purgeFrequency,
		updateInterval: updateInterval,
		updateFunc:     updateFunc,
		logger:         logger,
		cache:          gocache.New(gocache.NoExpiration, -1), // Manually purging and expiring these values based off custom logic
	}
	c.startWorkers()
	return c
}

func (c *cachex) startWorkers() {
	ctx := context.Background()
	reqID := fmt.Sprintf("%s-ALLOWANCE_WORKER", uuid.New().String())
	ctx = context.WithValue(ctx, contextx.UUIDKEY, reqID)

	go func() {
		c.startPurgeWorker(ctx)
		c.startUpdateWorker(ctx)
	}()
}

func (c *cachex) startPurgeWorker(ctx context.Context) {
	ticker := time.NewTicker(c.purgeFrequency)
	for range ticker.C {
		if err := c.purge(); err != nil {
			c.logger.WithFields(logrus.Fields{
				"id":  contextx.RequestID(ctx),
				"msg": err.Error(),
			}).Error("Failed to purge balance cache.")
		}
	}
}

func (c *cachex) purge() error {
	for key, item := range c.cache.Items() {
		allowanceItem, ok := item.Object.(allowanceItem)
		if !ok {
			return errors.New("failed to cast cache item into allowanceItem")
		}

		// if the last queried time plus the expiration interval is less than now, expired
		if allowanceItem.lastQueried.Add(c.expiration).Before(time.Now()) {
			c.cache.Delete(key)
		}
	}
	return nil
}

func (c *cachex) startUpdateWorker(ctx context.Context) {
	ticker := time.NewTicker(c.updateInterval)
	for range ticker.C {
		if err := c.update(ctx); err != nil {
			c.logger.WithFields(logrus.Fields{
				"id":  contextx.RequestID(ctx),
				"msg": err.Error(),
			}).Error("Failed to update allowance cache.")
		}
	}
}

func (c *cachex) update(ctx context.Context) error {
	for key, item := range c.cache.Items() {
		allowanceItem, ok := item.Object.(allowanceItem)
		if !ok {
			return errors.New("failed to cast cache item into allowanceItem")
		}

		allowed, err := c.updateFunc(ctx, cache.FromKey(key)...)
		if err != nil {
			return err
		}

		allowanceItem.allowed = allowed
		c.cache.Set(key, allowanceItem, gocache.NoExpiration)
	}
	return nil
}

// Set sets a allowance to a allowance cache where strs are a list of strings converted to a tuple as the key
func (c *cachex) Set(allowance string, strs ...string) {
	allowanceItem := allowanceItem{
		lastQueried: time.Now(),
		allowed:     allowance,
	}
	c.cache.Set(cache.ToKey(strs...), allowanceItem, gocache.NoExpiration)
}

// Get gets a allowance from a allowance cache where strs are a list of strings converted to a tuple as the key
func (c *cachex) Get(ctx context.Context, strs ...string) string {
	val, ok := c.cache.Get(cache.ToKey(strs...))
	if !ok {
		return c.notOK(ctx, strs...)
	}

	allowanceItem, ok := val.(allowanceItem)
	if !ok {
		return c.notOK(ctx, strs...)
	}

	allowanceItem.lastQueried = time.Now()
	c.cache.Set(cache.ToKey(strs...), allowanceItem, gocache.NoExpiration)

	return allowanceItem.allowed
}

func (c *cachex) notOK(ctx context.Context, strs ...string) string {
	allowance, err := c.updateFunc(ctx, strs...)
	if err != nil {
		return "0"
	}

	c.Set(allowance, strs...)
	return allowance
}
