package balance

import (
	"context"
	"errors"
	"fmt"
	"time"

	"github.com/google/uuid"
	gocache "github.com/patrickmn/go-cache"
	"github.com/sirupsen/logrus"
	"gitlab.com/camlcase-dev/indexter/internal/cache"
	contextx "gitlab.com/camlcase-dev/indexter/internal/context"
)

// Cache is a cache and worker for FA1.2 and FA2 balances
type Cache interface {
	Get(ctx context.Context, strs ...string) string
	Set(balance string, strs ...string)
}

type cachex struct {
	expiration     time.Duration
	purgeFrequency time.Duration
	updateInterval time.Duration
	updateFunc     func(ctx context.Context, params ...string) (string, error)
	logger         *logrus.Logger
	cache          *gocache.Cache
}

type balanceItem struct {
	lastQueried time.Time
	balance     string
}

// New returns a new balance Cache
func New(logger *logrus.Logger, expiration time.Duration, purgeFrequency time.Duration, updateInterval time.Duration, updateFunc func(ctx context.Context, params ...string) (string, error)) Cache {
	c := &cachex{
		expiration:     expiration,
		purgeFrequency: purgeFrequency,
		updateInterval: updateInterval,
		updateFunc:     updateFunc,
		logger:         logger,
		cache:          gocache.New(gocache.NoExpiration, -1), // Manually purging and expiring these values based off custom logic
	}
	c.startWorkers()
	return c
}

func (c *cachex) startWorkers() {
	ctx := context.Background()
	reqID := fmt.Sprintf("%s-BALANCE_WORKER", uuid.New().String())
	ctx = context.WithValue(ctx, contextx.UUIDKEY, reqID)

	go func() {
		c.startPurgeWorker(ctx)
		c.startUpdateWorker(ctx)
	}()
}

func (c *cachex) startPurgeWorker(ctx context.Context) {
	ticker := time.NewTicker(c.purgeFrequency)
	for range ticker.C {
		if err := c.purge(); err != nil {
			c.logger.WithFields(logrus.Fields{
				"id":  contextx.RequestID(ctx),
				"msg": err.Error(),
			}).Error("Failed to purge balance cache.")
		}
	}
}

func (c *cachex) purge() error {
	for key, item := range c.cache.Items() {
		balanceItem, ok := item.Object.(balanceItem)
		if !ok {
			return errors.New("failed to cast cache item into balanceItem")
		}

		// if the last queried time plus the expiration interval is less than now, expired
		if balanceItem.lastQueried.Add(c.expiration).Before(time.Now()) {
			c.cache.Delete(key)
		}
	}
	return nil
}

func (c *cachex) startUpdateWorker(ctx context.Context) {
	ticker := time.NewTicker(c.updateInterval)
	for range ticker.C {
		if err := c.update(ctx); err != nil {
			c.logger.WithFields(logrus.Fields{
				"id":  contextx.RequestID(ctx),
				"msg": err.Error(),
			}).Error("Failed to update balance cache.")
		}
	}
}

func (c *cachex) update(ctx context.Context) error {
	for key, item := range c.cache.Items() {
		balanceItem, ok := item.Object.(balanceItem)
		if !ok {
			return errors.New("failed to cast cache item into balanceItem")
		}

		balance, err := c.updateFunc(ctx, cache.FromKey(key)...)
		if err != nil {
			return err
		}

		balanceItem.balance = balance
		c.cache.Set(key, balanceItem, gocache.NoExpiration)
	}
	return nil
}

// Set sets a balance to a balance cache where strs are a list of strings converted to a tuple as the key
func (c *cachex) Set(balance string, strs ...string) {
	balanceItem := balanceItem{
		lastQueried: time.Now(),
		balance:     balance,
	}
	c.cache.Set(cache.ToKey(strs...), balanceItem, gocache.NoExpiration)
}

// Get gets a balance from a balance cache where strs are a list of strings converted to a tuple as the key
func (c *cachex) Get(ctx context.Context, strs ...string) string {
	val, ok := c.cache.Get(cache.ToKey(strs...))
	if !ok {
		return c.notOK(ctx, strs...)
	}

	balanceItem, ok := val.(balanceItem)
	if !ok {
		return c.notOK(ctx, strs...)
	}

	balanceItem.lastQueried = time.Now()
	c.cache.Set(cache.ToKey(strs...), balanceItem, gocache.NoExpiration)

	return balanceItem.balance
}

func (c *cachex) notOK(ctx context.Context, strs ...string) string {
	balance, err := c.updateFunc(ctx, strs...)
	if err != nil {
		return "0"
	}

	c.Set(balance, strs...)
	return balance
}
