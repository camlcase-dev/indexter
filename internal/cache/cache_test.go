package cache

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_Keys(t *testing.T) {
	type input struct {
		strs []string
	}

	type want struct {
		key string
	}

	cases := []struct {
		name  string
		input input
		want  want
	}{
		{
			"works with pair",
			input{
				[]string{"a", "b"},
			},
			want{
				"(a,b)",
			},
		},
		{
			"works with 3 tuple",
			input{
				[]string{"a", "b", "c"},
			},
			want{
				"(a,b,c)",
			},
		},
		{
			"works with 4 tuple",
			input{
				[]string{"a", "b", "c", "d"},
			},
			want{
				"(a,b,c,d)",
			},
		},
		{
			"works with single",
			input{
				[]string{"a"},
			},
			want{
				"(a)",
			},
		},
	}

	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			key := ToKey(tt.input.strs...)
			assert.Equal(t, tt.want.key, key)
			strs := FromKey(key)
			assert.Equal(t, tt.input.strs, strs)
		})
	}
}
