package fa12

import (
	"context"
	"time"

	"github.com/go-resty/resty/v2"
	"github.com/goat-systems/go-tezos/v4/rpc"
	"github.com/sirupsen/logrus"
	"gitlab.com/camlcase-dev/indexter/internal/cache/allowance"
	"gitlab.com/camlcase-dev/indexter/internal/cache/balance"
	"gitlab.com/camlcase-dev/indexter/internal/cache/supply"
	contextx "gitlab.com/camlcase-dev/indexter/internal/context"
	errorsx "gitlab.com/camlcase-dev/indexter/internal/errors"
)

// FA12 is a cache containing fa1.2 info
type FA12 struct {
	supplyCache    supply.Cache
	balanceCache   balance.Cache
	allowanceCache allowance.Cache

	contractViewAddress string
	source              string

	rpc    rpc.IFace
	logger *logrus.Logger
}

// New returns an FA1.2 balance cache
func New(logger *logrus.Logger, expiration time.Duration, purge time.Duration, updateInterval time.Duration, contractViewAddress, source string, rpcClient rpc.IFace) FA12 {
	fa12 := FA12{

		contractViewAddress: contractViewAddress,
		source:              source,

		rpc:    rpcClient,
		logger: logger,
	}

	fa12.balanceCache = balance.New(logger, expiration, purge, updateInterval, fa12.getBalanceWithRetry)
	fa12.supplyCache = supply.New(logger, updateInterval, fa12.getSupplyWithRetry)
	fa12.allowanceCache = allowance.New(logger, expiration, purge, updateInterval, fa12.getAllowanceWithRetry)
	return fa12
}

// GetBalanceWithCache will get the fa1.2 balance for an address by first attempting to do a lookup in the cache
// if no balance is found in the cache one will be added.
func (f *FA12) GetBalanceWithCache(ctx context.Context, contract, address string) string {
	return f.balanceCache.Get(ctx, contract, address)
}

// GetBalance will not use the cache and do a direct lookup of an fa1.2 balance at the tezos-node
func (f *FA12) GetBalance(ctx context.Context, contract, address string) (string, error) {
	return f.getBalanceWithRetry(ctx, contract, address)
}

func (f *FA12) getBalanceWithRetry(ctx context.Context, strs ...string) (string, error) {
	balance, err := f.getBalance(ctx, strs[0], strs[1])
	if err != nil {
		ticker := time.NewTicker(time.Millisecond * 50)
		tries := 10
		for range ticker.C {
			balance, err := f.getBalance(ctx, strs[0], strs[1])
			if err != nil {
				tries = tries - 1
				if tries == 0 {
					return "0", err
				}
			} else {
				return balance, nil
			}
		}
	}

	return balance, nil
}

func (f *FA12) getBalance(ctx context.Context, contract, address string) (string, error) {
	resp, head, err := f.rpc.Block(&rpc.BlockIDHead{})
	f.logNodeResponse(ctx, resp)
	if err != nil {
		return "0", errorsx.FromResponse(ctx, resp, err.Error())
	}

	blockID := rpc.BlockIDHash(head.Hash)

	// contract, address
	resp, balance, err := f.rpc.GetFA12Balance(rpc.GetFA12BalanceInput{
		BlockID:             &blockID,
		ChainID:             head.ChainID,
		FA12Contract:        contract,
		OwnerAddress:        address,
		ContractViewAddress: f.contractViewAddress,
		Source:              f.source,
	})
	f.logNodeResponse(ctx, resp)
	if err != nil {
		return "0", errorsx.FromResponse(ctx, resp, err.Error())
	}

	f.balanceCache.Set(balance, contract, address)
	return balance, nil
}

// GetSupplyWithCache will return the supply of an FA1.2 contract from the cache
func (f *FA12) GetSupplyWithCache(ctx context.Context, contract string) string {
	return f.supplyCache.Get(ctx, contract)
}

// GetSupply will return the supply of an FA1.2 contract
func (f *FA12) GetSupply(ctx context.Context, contract string) (string, error) {
	return f.getSupplyWithRetry(ctx, contract)
}

func (f *FA12) getSupplyWithRetry(ctx context.Context, strs ...string) (string, error) {
	supply, err := f.getSupply(ctx, strs[0])
	if err != nil {
		ticker := time.NewTicker(time.Millisecond * 50)
		tries := 10
		for range ticker.C {
			supply, err := f.getSupply(ctx, strs[0])
			if err != nil {
				tries = tries - 1
				if tries == 0 {
					return "0", err
				}
			} else {
				return supply, nil
			}
		}
	}

	return supply, nil
}

// GetSupply gets the supply for an fa1.2 contract
func (f *FA12) getSupply(ctx context.Context, contract string) (string, error) {
	resp, head, err := f.rpc.Block(&rpc.BlockIDHead{})
	f.logNodeResponse(ctx, resp)
	if err != nil {
		return "0", errorsx.FromResponse(ctx, resp, err.Error())
	}

	blockID := rpc.BlockIDHash(head.Hash)
	resp, supply, err := f.rpc.GetFA12Supply(rpc.GetFA12SupplyInput{
		BlockID:             &blockID,
		ChainID:             head.ChainID,
		Source:              f.source,
		FA12Contract:        contract,
		ContractViewAddress: f.contractViewAddress,
	})
	f.logNodeResponse(ctx, resp)
	if err != nil {
		return "0", errorsx.FromResponse(ctx, resp, err.Error())
	}

	f.supplyCache.Set(supply, contract)
	return supply, nil
}

// GetAllowanceWithCache -
func (f *FA12) GetAllowanceWithCache(ctx context.Context, contract, owner, spender string) string {
	return f.allowanceCache.Get(ctx, contract, owner, spender)
}

// GetAllowance -
func (f *FA12) GetAllowance(ctx context.Context, contract, owner, spender string) (string, error) {
	return f.getAllowanceWithRetry(ctx, contract, owner, spender)
}

func (f *FA12) getAllowanceWithRetry(ctx context.Context, strs ...string) (string, error) {
	allowance, err := f.getAllowance(ctx, strs[0], strs[1], strs[2])
	if err != nil {
		ticker := time.NewTicker(time.Millisecond * 50)
		tries := 10
		for range ticker.C {
			supply, err := f.getAllowance(ctx, strs[0], strs[1], strs[2])
			if err != nil {
				tries = tries - 1
				if tries == 0 {
					return "0", err
				}
			} else {
				return supply, nil
			}
		}
	}

	return allowance, nil
}

func (f *FA12) getAllowance(ctx context.Context, contract, owner, spender string) (string, error) {
	resp, head, err := f.rpc.Block(&rpc.BlockIDHead{})
	f.logNodeResponse(ctx, resp)
	if err != nil {
		return "0", errorsx.FromResponse(ctx, resp, err.Error())
	}
	blockID := rpc.BlockIDHash(head.Hash)

	resp, allowance, err := f.rpc.GetFA12Allowance(rpc.GetFA12AllowanceInput{
		BlockID:             &blockID,
		ChainID:             head.ChainID,
		Source:              f.source,
		FA12Contract:        contract,
		OwnerAddress:        owner,
		SpenderAddress:      spender,
		ContractViewAddress: f.contractViewAddress,
	})
	f.logNodeResponse(ctx, resp)
	if err != nil {
		return "0", errorsx.FromResponse(ctx, resp, err.Error())
	}

	f.allowanceCache.Set(allowance, contract, owner, spender)
	return allowance, nil
}

func (f *FA12) logNodeResponse(ctx context.Context, resp *resty.Response) {
	reqID := contextx.RequestID(ctx)
	network := contextx.NetworkID(ctx)
	if resp != nil {
		f.logger.WithFields(logrus.Fields{
			"request_id":      reqID,
			"network":         network,
			"response_body":   string(resp.Body()),
			"response_status": resp.StatusCode(),
			"request_url":     respURL(resp),
			"request_method":  respMethod(resp),
			"request_body":    respBody(resp),
		}).Info("Tezos node response received.")
	} else {
		f.logger.WithFields(logrus.Fields{
			"request_id": reqID,
			"network":    network,
		}).Info("Tezos node did not receive response.")
	}
}

func respURL(resp *resty.Response) string {
	if resp != nil {
		if resp.Request != nil {
			return resp.Request.URL
		}
	}

	return ""
}

func respMethod(resp *resty.Response) string {
	if resp != nil {
		if resp.Request != nil {
			return resp.Request.Method
		}
	}

	return ""
}

func respBody(resp *resty.Response) interface{} {
	if resp != nil {
		if resp.Request != nil {
			return resp.Request.Body
		}
	}

	return nil
}
