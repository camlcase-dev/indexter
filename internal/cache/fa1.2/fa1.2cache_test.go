package fa12

import (
	"context"
	"errors"
	"fmt"
	"testing"
	"time"

	"github.com/go-resty/resty/v2"
	"github.com/goat-systems/go-tezos/v4/rpc"
	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	contextx "gitlab.com/camlcase-dev/indexter/internal/context"
)

func Test_GetBalance(t *testing.T) {
	ctx := testContext()
	logger := logrus.New()

	type input struct {
		contract string
		owner    string
		r        rpc.IFace
	}

	type want struct {
		err      bool
		contains string
		balance  string
	}

	cases := []struct {
		name  string
		input input
		want  want
	}{
		{
			"is successful",
			input{
				"some_contract",
				"some_owner",
				&RPCMock{
					headErr:        false,
					getFA12Balance: false,
				},
			},
			want{
				false,
				"",
				"150000",
			},
		},
		{
			"handles failure",
			input{
				"some_contract",
				"some_owner",
				&RPCMock{
					headErr:        true,
					getFA12Balance: false,
				},
			},
			want{
				true,
				"failed to get block",
				"0",
			},
		},
	}

	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			fa2 := New(logger, time.Millisecond, time.Millisecond*3, time.Millisecond*3, "view", "source", tt.input.r)

			balance, err := fa2.GetBalance(ctx, tt.input.contract, tt.input.owner)
			checkErr(t, tt.want.err, tt.want.contains, err)
			assert.Equal(t, tt.want.balance, balance)

			balance = fa2.GetBalanceWithCache(ctx, tt.input.contract, tt.input.owner)
			assert.Equal(t, tt.want.balance, balance)
		})
	}
}

func Test_GetSupply(t *testing.T) {
	ctx := testContext()
	logger := logrus.New()

	type input struct {
		contract string
		r        rpc.IFace
	}

	type want struct {
		err      bool
		contains string
		supply   string
	}

	cases := []struct {
		name  string
		input input
		want  want
	}{
		{
			"is successful",
			input{
				"some_contract",
				&RPCMock{
					headErr:       false,
					getFA12Supply: false,
				},
			},
			want{
				false,
				"",
				"100000",
			},
		},
		{
			"handles failure",
			input{
				"some_contract",
				&RPCMock{
					headErr:       true,
					getFA12Supply: false,
				},
			},
			want{
				true,
				"failed to get block",
				"0",
			},
		},
	}

	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			fa2 := New(logger, time.Millisecond, time.Millisecond*3, time.Millisecond*3, "view", "source", tt.input.r)

			supply, err := fa2.GetSupply(ctx, tt.input.contract)
			checkErr(t, tt.want.err, tt.want.contains, err)
			assert.Equal(t, tt.want.supply, supply)

			supply = fa2.GetSupplyWithCache(ctx, tt.input.contract)
			assert.Equal(t, tt.want.supply, supply)
		})
	}
}

func Test_GetAllowance(t *testing.T) {
	ctx := testContext()
	logger := logrus.New()

	type input struct {
		contract string
		owner    string
		operator string
		r        rpc.IFace
	}

	type want struct {
		err       bool
		contains  string
		allowance string
	}

	cases := []struct {
		name  string
		input input
		want  want
	}{
		{
			"is successful",
			input{
				"some_contract",
				"some_owner",
				"some_operator",
				&RPCMock{
					headErr:          false,
					getFA12Allowance: false,
				},
			},
			want{
				false,
				"",
				"100000",
			},
		},
		{
			"handles failure",
			input{
				"some_contract",
				"some_owner",
				"some_operator",
				&RPCMock{
					headErr:          true,
					getFA12Allowance: false,
				},
			},
			want{
				true,
				"failed to get block",
				"0",
			},
		},
	}

	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			fa2 := New(logger, time.Millisecond, time.Millisecond*3, time.Millisecond*3, "view", "source", tt.input.r)

			allowance, err := fa2.GetAllowance(ctx, tt.input.contract, tt.input.owner, tt.input.operator)
			checkErr(t, tt.want.err, tt.want.contains, err)
			assert.Equal(t, tt.want.allowance, allowance)

			allowance = fa2.GetAllowanceWithCache(ctx, tt.input.contract, tt.input.owner, tt.input.operator)
			assert.Equal(t, tt.want.allowance, allowance)
		})
	}
}

func testContext() context.Context {
	ctx := context.Background()
	reqID := fmt.Sprintf("%s-TEST", uuid.New().String())
	ctx = context.WithValue(ctx, contextx.UUIDKEY, reqID)
	return context.WithValue(ctx, contextx.NETWORKKEY, "testnet")
}

func checkErr(t *testing.T, wantErr bool, errContains string, err error) {
	if wantErr {
		assert.Error(t, err)
		if err != nil {
			assert.Contains(t, err.Error(), errContains)
		}
	} else {
		assert.Nil(t, err)
	}
}

var _ rpc.IFace = &RPCMock{}

type RPCMock struct {
	rpc.IFace
	headErr          bool
	getFA12Supply    bool
	getFA12Balance   bool
	getFA12Allowance bool
}

func (r *RPCMock) Block(rpc.BlockID) (*resty.Response, *rpc.Block, error) {
	if r.headErr {
		return &resty.Response{}, &rpc.Block{}, errors.New("failed to get block")
	}

	return &resty.Response{}, &rpc.Block{
		Hash:    "some_hash",
		ChainID: "some_chain_id",
	}, nil
}

func (r *RPCMock) GetFA12Balance(input rpc.GetFA12BalanceInput) (*resty.Response, string, error) {
	if r.getFA12Balance {
		return &resty.Response{}, "0", errors.New("failed to get balance")
	}

	return &resty.Response{}, "150000", nil
}

func (r *RPCMock) GetFA12Supply(input rpc.GetFA12SupplyInput) (*resty.Response, string, error) {
	if r.getFA12Supply {
		return &resty.Response{}, "0", errors.New("failed to get supply")
	}

	return &resty.Response{}, "100000", nil
}

func (r *RPCMock) GetFA12Allowance(input rpc.GetFA12AllowanceInput) (*resty.Response, string, error) {
	if r.getFA12Allowance {
		return &resty.Response{}, "0", errors.New("failed to get allowance")
	}

	return &resty.Response{}, "100000", nil
}
