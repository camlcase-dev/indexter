package fa2

import (
	"encoding/json"
	"fmt"

	"github.com/go-resty/resty/v2"
	"github.com/pkg/errors"
)

type client interface {
	tokens(contract string) (*resty.Response, []string, error)
	supply(contract, id string) (*resty.Response, string, error)
	permissions(contract, id, owner, operator string) (*resty.Response, bool, error)
	balance(contract, id, owner string) (*resty.Response, string, error)
}

type fa2Client struct {
	r       *resty.Client
	host    string
	network string
}

func newClient(host, network string) client {
	r := resty.New()
	return &fa2Client{
		r:    r,
		host: host,
	}
}

func (f *fa2Client) tokens(contract string) (*resty.Response, []string, error) {
	resp, err := f.r.R().Get(fmt.Sprintf("%s/fa2/%s/tokens?network=%s", f.host, contract, f.network))
	if err != nil {
		return resp, []string{}, errors.Wrap(err, "failed to get response from host")
	}

	var tokens []string
	err = json.Unmarshal(resp.Body(), &tokens)
	if err != nil {
		return resp, []string{}, errors.Wrap(err, "failed to parse body")
	}

	return resp, tokens, nil
}

func (f *fa2Client) supply(contract string, id string) (*resty.Response, string, error) {
	resp, err := f.r.R().Get(fmt.Sprintf("%s/fa2/%s/%s/total/supply?network=%s", f.host, contract, id, f.network))
	if err != nil {
		return resp, "", errors.Wrap(err, "failed to get response from host")
	}

	var supply string
	err = json.Unmarshal(resp.Body(), &supply)
	if err != nil {
		return resp, "", errors.Wrap(err, "failed to parse body")
	}

	return resp, supply, nil
}

func (f *fa2Client) permissions(contract, id, owner, operator string) (*resty.Response, bool, error) {
	resp, err := f.r.R().Get(fmt.Sprintf("%s/fa2/%s/%s/permission/%s/%s?network=%s", f.host, contract, id, owner, operator, f.network))
	if err != nil {
		return resp, false, errors.Wrap(err, "failed to get response from host")
	}

	var permission bool
	err = json.Unmarshal(resp.Body(), &permission)
	if err != nil {
		return resp, false, errors.Wrap(err, "failed to parse body")
	}

	return resp, permission, nil
}

func (f *fa2Client) balance(contract, id, owner string) (*resty.Response, string, error) {
	resp, err := f.r.R().Get(fmt.Sprintf("%s/fa2/%s/%s/balance/%s?network=%s", f.host, contract, id, owner, f.network))
	if err != nil {
		return resp, "", errors.Wrap(err, "failed to get response from host")
	}

	var balance string
	err = json.Unmarshal(resp.Body(), &balance)
	if err != nil {
		return resp, "", errors.Wrap(err, "failed to parse body")
	}

	return resp, balance, nil
}
