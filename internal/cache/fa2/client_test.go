package fa2

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_tokens(t *testing.T) {
	goldenHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte(`["0"]`))
	})

	parseFailureHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte(`some_error`))
	})

	type input struct {
		handler http.Handler
	}

	type want struct {
		err         bool
		errContains string
		tokens      []string
	}

	cases := []struct {
		name  string
		input input
		want  want
	}{
		{
			"is successful",
			input{
				goldenHandler,
			},
			want{
				false,
				"",
				[]string{
					"0",
				},
			},
		},
		{
			"handles failure to parse",
			input{
				parseFailureHandler,
			},
			want{
				true,
				"failed to parse body",
				[]string{},
			},
		},
	}

	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			server := httptest.NewServer(tt.input.handler)
			defer server.Close()

			c := newClient(server.URL, "mainnet")
			_, tokens, err := c.tokens("some_contract")

			checkErr(t, tt.want.err, tt.want.errContains, err)
			assert.Equal(t, tt.want.tokens, tokens)
		})
	}
}

func Test_supply(t *testing.T) {
	goldenHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte(`"10000000"`))
	})

	parseFailureHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte(`some_error`))
	})

	type input struct {
		handler http.Handler
	}

	type want struct {
		err         bool
		errContains string
		supply      string
	}

	cases := []struct {
		name  string
		input input
		want  want
	}{
		{
			"is successful",
			input{
				goldenHandler,
			},
			want{
				false,
				"",
				"10000000",
			},
		},
		{
			"handles failure to parse",
			input{
				parseFailureHandler,
			},
			want{
				true,
				"failed to parse body",
				"",
			},
		},
	}

	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			server := httptest.NewServer(tt.input.handler)
			defer server.Close()

			c := newClient(server.URL, "mainnet")
			_, supply, err := c.supply("some_contract", "0")

			checkErr(t, tt.want.err, tt.want.errContains, err)
			assert.Equal(t, tt.want.supply, supply)
		})
	}
}

func Test_permissions(t *testing.T) {
	goldenHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte(`true`))
	})

	parseFailureHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte(`some_error`))
	})

	type input struct {
		handler http.Handler
	}

	type want struct {
		err         bool
		errContains string
		permission  bool
	}

	cases := []struct {
		name  string
		input input
		want  want
	}{
		{
			"is successful",
			input{
				goldenHandler,
			},
			want{
				false,
				"",
				true,
			},
		},
		{
			"handles failure to parse",
			input{
				parseFailureHandler,
			},
			want{
				true,
				"failed to parse body",
				false,
			},
		},
	}

	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			server := httptest.NewServer(tt.input.handler)
			defer server.Close()

			c := newClient(server.URL, "mainnet")
			_, permission, err := c.permissions("some_contract", "0", "some_owner", "some_operator")

			checkErr(t, tt.want.err, tt.want.errContains, err)
			assert.Equal(t, tt.want.permission, permission)
		})
	}
}

func Test_balance(t *testing.T) {
	goldenHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte(`"10000000"`))
	})

	parseFailureHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte(`some_error`))
	})

	type input struct {
		handler http.Handler
	}

	type want struct {
		err         bool
		errContains string
		balance     string
	}

	cases := []struct {
		name  string
		input input
		want  want
	}{
		{
			"is successful",
			input{
				goldenHandler,
			},
			want{
				false,
				"",
				"10000000",
			},
		},
		{
			"handles failure to parse",
			input{
				parseFailureHandler,
			},
			want{
				true,
				"failed to parse body",
				"",
			},
		},
	}

	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			server := httptest.NewServer(tt.input.handler)
			defer server.Close()

			c := newClient(server.URL, "mainnet")
			_, balance, err := c.balance("some_contract", "0", "some_owner")

			checkErr(t, tt.want.err, tt.want.errContains, err)
			assert.Equal(t, tt.want.balance, balance)
		})
	}
}

func checkErr(t *testing.T, wantErr bool, errContains string, err error) {
	if wantErr {
		assert.Error(t, err)
		if err != nil {
			assert.Contains(t, err.Error(), errContains)
		}
	} else {
		assert.Nil(t, err)
	}
}
