package fa2

import (
	"context"
	"time"

	"github.com/go-resty/resty/v2"
	"github.com/sirupsen/logrus"

	"gitlab.com/camlcase-dev/indexter/internal/cache/balance"
	"gitlab.com/camlcase-dev/indexter/internal/cache/permission"
	"gitlab.com/camlcase-dev/indexter/internal/cache/supply"
	"gitlab.com/camlcase-dev/indexter/internal/cache/tokenid"
	contextx "gitlab.com/camlcase-dev/indexter/internal/context"
	errorsx "gitlab.com/camlcase-dev/indexter/internal/errors"
)

// FA2 is a cache containing FA2 info
type FA2 struct {
	supplyCache     supply.Cache
	tokenIDCache    tokenid.Cache
	balanceCache    balance.Cache
	permissionCache permission.Cache

	c      client
	logger *logrus.Logger
}

// New returns an FA1.2 balance cache
func New(logger *logrus.Logger, expiration time.Duration, purge time.Duration, updateInterval time.Duration, host, network string) FA2 {
	fa2 := FA2{
		c:      newClient(host, network),
		logger: logger,
	}

	fa2.supplyCache = supply.New(logger, updateInterval, fa2.getSupplyWithRetry)
	fa2.tokenIDCache = tokenid.New(logger, updateInterval, fa2.getTokensWithRetry)
	fa2.balanceCache = balance.New(logger, expiration, purge, updateInterval, fa2.getBalanceWithRetry)
	fa2.permissionCache = permission.New(logger, expiration, purge, updateInterval, fa2.getPermissionWithRetry)

	return fa2
}

// GetBalanceWithCache will get the fa1.2 balance for an address by first attempting to do a lookup in the cache
// if no balance is found in the cache one will be added.
func (f *FA2) GetBalanceWithCache(ctx context.Context, contract, tokenID, owner string) string {
	return f.balanceCache.Get(ctx, contract, tokenID, owner)
}

// GetBalance will not use the cache and do a direct lookup of an fa1.2 balance at the tezos-node
func (f *FA2) GetBalance(ctx context.Context, contract, tokenID, owner string) (string, error) {
	return f.getBalanceWithRetry(ctx, contract, tokenID, owner)
}

func (f *FA2) getBalanceWithRetry(ctx context.Context, strs ...string) (string, error) {
	balance, err := f.getBalance(ctx, strs[0], strs[1], strs[2])
	if err != nil {
		ticker := time.NewTicker(time.Millisecond * 50)
		tries := 10
		for range ticker.C {
			balance, err := f.getBalance(ctx, strs[0], strs[1], strs[2])
			if err != nil {
				tries = tries - 1
				if tries == 0 {
					return "0", err
				}
			} else {
				return balance, nil
			}
		}
	}

	return balance, nil
}

func (f *FA2) getBalance(ctx context.Context, contract, tokenID, owner string) (string, error) {
	resp, balance, err := f.c.balance(contract, tokenID, owner)
	f.logResponse(ctx, resp)
	if err != nil {
		return "0", errorsx.FromResponse(ctx, resp, err.Error())
	}

	f.balanceCache.Set(balance, contract, tokenID, owner)
	return balance, nil
}

// GetSupplyWithCache will return the supply of an FA1.2 contract from the cache
func (f *FA2) GetSupplyWithCache(ctx context.Context, contract, tokenID string) string {
	return f.supplyCache.Get(ctx, contract, tokenID)
}

// GetSupply will return the supply of an FA1.2 contract
func (f *FA2) GetSupply(ctx context.Context, contract, tokenID string) (string, error) {
	return f.getSupplyWithRetry(ctx, contract, tokenID)
}

func (f *FA2) getSupplyWithRetry(ctx context.Context, strs ...string) (string, error) {
	supply, err := f.getSupply(ctx, strs[0], strs[1])
	if err != nil {
		ticker := time.NewTicker(time.Millisecond * 50)
		tries := 10
		for range ticker.C {
			supply, err := f.getSupply(ctx, strs[0], strs[1])
			if err != nil {
				tries = tries - 1
				if tries == 0 {
					return "0", err
				}
			} else {
				return supply, nil
			}
		}
	}

	return supply, nil
}

// GetSupply gets the supply for an fa1.2 contract
func (f *FA2) getSupply(ctx context.Context, contract, tokenID string) (string, error) {
	resp, supply, err := f.c.supply(contract, tokenID)
	f.logResponse(ctx, resp)
	if err != nil {
		return "0", errorsx.FromResponse(ctx, resp, err.Error())
	}

	f.supplyCache.Set(supply, contract, tokenID)
	return supply, nil
}

// GetPermissionWithCache will return the permission of an FA2 token from the cache
func (f *FA2) GetPermissionWithCache(ctx context.Context, contract, tokenID, owner, operator string) bool {
	return f.permissionCache.Get(ctx, contract, contract, tokenID, owner, operator)
}

// GetPermission -
func (f *FA2) GetPermission(ctx context.Context, contract, tokenID, owner, operator string) (bool, error) {
	return f.getPermissionWithRetry(ctx, contract, tokenID, owner, operator)
}

func (f *FA2) getPermissionWithRetry(ctx context.Context, strs ...string) (bool, error) {
	permission, err := f.getPermission(ctx, strs[0], strs[1], strs[2], strs[3])
	if err != nil {
		ticker := time.NewTicker(time.Millisecond * 50)
		tries := 10
		for range ticker.C {
			permission, err := f.getPermission(ctx, strs[0], strs[1], strs[2], strs[3])
			if err != nil {
				tries = tries - 1
				if tries == 0 {
					return false, err
				}
			} else {
				return permission, nil
			}
		}
	}

	return permission, nil
}

func (f *FA2) getPermission(ctx context.Context, contract, tokenID, owner, operator string) (bool, error) {
	resp, permission, err := f.c.permissions(contract, tokenID, owner, operator)
	f.logResponse(ctx, resp)
	if err != nil {
		return false, errorsx.FromResponse(ctx, resp, err.Error())
	}

	f.permissionCache.Set(permission, contract, tokenID, owner, operator)
	return permission, nil
}

// GetTokensWithCache will return the tokens of an FA2 contract from the cache
func (f *FA2) GetTokensWithCache(ctx context.Context, contract string) []string {
	return f.tokenIDCache.Get(ctx, contract, contract)
}

// GetTokens will return the tokens of an FA2 contract
func (f *FA2) GetTokens(ctx context.Context, contract string) ([]string, error) {
	return f.getTokensWithRetry(ctx, contract)
}

func (f *FA2) getTokensWithRetry(ctx context.Context, strs ...string) ([]string, error) {
	tokens, err := f.getTokens(ctx, strs[0])
	if err != nil {
		ticker := time.NewTicker(time.Millisecond * 50)
		tries := 10
		for range ticker.C {
			tokens, err := f.getTokens(ctx, strs[0])
			if err != nil {
				tries = tries - 1
				if tries == 0 {
					return []string{}, err
				}
			} else {
				return tokens, nil
			}
		}
	}

	return tokens, nil
}

func (f *FA2) getTokens(ctx context.Context, contract string) ([]string, error) {
	resp, tokenIDs, err := f.c.tokens(contract)
	f.logResponse(ctx, resp)
	if err != nil {
		return []string{}, errorsx.FromResponse(ctx, resp, err.Error())
	}

	f.tokenIDCache.Set(tokenIDs, contract)
	return tokenIDs, nil
}

func (f *FA2) logResponse(ctx context.Context, resp *resty.Response) {
	reqID := contextx.RequestID(ctx)
	network := contextx.NetworkID(ctx)
	if resp != nil {
		f.logger.WithFields(logrus.Fields{
			"request_id":      reqID,
			"network":         network,
			"response_body":   string(resp.Body()),
			"response_status": resp.StatusCode(),
			"request_url":     resp.Request.URL,
			"request_method":  resp.Request.Method,
			"request_body":    resp.Request.Body,
		}).Info("Response received from FA2 server.")
	} else {
		f.logger.WithFields(logrus.Fields{
			"request_id": reqID,
			"network":    network,
		}).Info("Did not receive response from FA2 server.")
	}
}
