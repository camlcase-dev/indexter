package fa2

import (
	"context"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	contextx "gitlab.com/camlcase-dev/indexter/internal/context"
)

func Test_GetBalance(t *testing.T) {
	ctx := testContext()
	logger := logrus.New()

	type input struct {
		contract string
		tokenID  string
		owner    string
		handler  http.Handler
	}

	type want struct {
		err      bool
		contains string
		balance  string
	}

	cases := []struct {
		name  string
		input input
		want  want
	}{
		{
			"is successful",
			input{
				"some_contract",
				"0",
				"some_owner",
				http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
					w.Write([]byte(`"10000000"`))
				}),
			},
			want{
				false,
				"",
				"10000000",
			},
		},
		{
			"handles failure",
			input{
				"some_contract",
				"0",
				"some_owner",
				http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
					w.Write([]byte(`some_error`))
				}),
			},
			want{
				true,
				"failed to parse body",
				"0",
			},
		},
	}

	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			server := httptest.NewServer(tt.input.handler)
			defer server.Close()

			fa2 := New(logger, time.Millisecond, time.Millisecond*3, time.Millisecond*3, server.URL, "mainnet")

			balance, err := fa2.GetBalance(ctx, tt.input.contract, tt.input.tokenID, tt.input.owner)
			checkErr(t, tt.want.err, tt.want.contains, err)
			assert.Equal(t, tt.want.balance, balance)

			balance = fa2.GetBalanceWithCache(ctx, tt.input.contract, tt.input.tokenID, tt.input.owner)
			assert.Equal(t, tt.want.balance, balance)
		})
	}
}

func Test_GetSupply(t *testing.T) {
	ctx := testContext()
	logger := logrus.New()

	type input struct {
		contract string
		tokenID  string
		handler  http.Handler
	}

	type want struct {
		err      bool
		contains string
		supply   string
	}

	cases := []struct {
		name  string
		input input
		want  want
	}{
		{
			"is successful",
			input{
				"some_contract",
				"0",
				http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
					w.Write([]byte(`"10000000"`))
				}),
			},
			want{
				false,
				"",
				"10000000",
			},
		},
		{
			"handles failure",
			input{
				"some_contract",
				"0",
				http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
					w.Write([]byte(`some_error`))
				}),
			},
			want{
				true,
				"failed to parse body",
				"0",
			},
		},
	}

	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			server := httptest.NewServer(tt.input.handler)
			defer server.Close()

			fa2 := New(logger, time.Millisecond, time.Millisecond*3, time.Millisecond*3, server.URL, "mainnet")

			supply, err := fa2.GetSupply(ctx, tt.input.contract, tt.input.tokenID)
			checkErr(t, tt.want.err, tt.want.contains, err)
			assert.Equal(t, tt.want.supply, supply)

			supply = fa2.GetSupplyWithCache(ctx, tt.input.contract, tt.input.tokenID)
			assert.Equal(t, tt.want.supply, supply)
		})
	}
}

func Test_GetPermission(t *testing.T) {
	ctx := testContext()
	logger := logrus.New()

	type input struct {
		contract string
		tokenID  string
		owner    string
		operator string
		handler  http.Handler
	}

	type want struct {
		err        bool
		contains   string
		permission bool
	}

	cases := []struct {
		name  string
		input input
		want  want
	}{
		{
			"is successful",
			input{
				"some_contract",
				"0",
				"some_owner",
				"some_operator",
				http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
					w.Write([]byte(`true`))
				}),
			},
			want{
				false,
				"",
				true,
			},
		},
		{
			"handles failure",
			input{
				"some_contract",
				"0",
				"some_owner",
				"some_operator",
				http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
					w.Write([]byte(`some_error`))
				}),
			},
			want{
				true,
				"failed to parse body",
				false,
			},
		},
	}

	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			server := httptest.NewServer(tt.input.handler)
			defer server.Close()

			fa2 := New(logger, time.Millisecond, time.Millisecond*3, time.Millisecond*3, server.URL, "mainnet")

			permission, err := fa2.GetPermission(ctx, tt.input.contract, tt.input.tokenID, tt.input.owner, tt.input.operator)
			checkErr(t, tt.want.err, tt.want.contains, err)
			assert.Equal(t, tt.want.permission, permission)

			permission = fa2.GetPermissionWithCache(ctx, tt.input.contract, tt.input.tokenID, tt.input.owner, tt.input.operator)
			assert.Equal(t, tt.want.permission, permission)
		})
	}
}

func Test_GetTokenIDs(t *testing.T) {
	ctx := testContext()
	logger := logrus.New()

	type input struct {
		contract string
		handler  http.Handler
	}

	type want struct {
		err      bool
		contains string
		tokenIDs []string
	}

	cases := []struct {
		name  string
		input input
		want  want
	}{
		{
			"is successful",
			input{
				"some_contract",
				http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
					w.Write([]byte(`["0","1"]`))
				}),
			},
			want{
				false,
				"",
				[]string{"0", "1"},
			},
		},
		{
			"handles failure",
			input{
				"some_contract",
				http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
					w.Write([]byte(`some_error`))
				}),
			},
			want{
				true,
				"failed to parse body",
				[]string{},
			},
		},
	}

	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			server := httptest.NewServer(tt.input.handler)
			defer server.Close()

			fa2 := New(logger, time.Millisecond, time.Millisecond*3, time.Millisecond*3, server.URL, "mainnet")

			tokenIDs, err := fa2.GetTokens(ctx, tt.input.contract)
			checkErr(t, tt.want.err, tt.want.contains, err)
			assert.Equal(t, tt.want.tokenIDs, tokenIDs)

			tokenIDs = fa2.GetTokensWithCache(ctx, tt.input.contract)
			assert.Equal(t, tt.want.tokenIDs, tokenIDs)
		})
	}
}

func testContext() context.Context {
	ctx := context.Background()
	reqID := fmt.Sprintf("%s-TEST", uuid.New().String())
	ctx = context.WithValue(ctx, contextx.UUIDKEY, reqID)
	return context.WithValue(ctx, contextx.NETWORKKEY, "testnet")
}
