package permission

import (
	"context"
	"errors"
	"fmt"
	"time"

	"github.com/google/uuid"
	gocache "github.com/patrickmn/go-cache"
	"github.com/sirupsen/logrus"
	"gitlab.com/camlcase-dev/indexter/internal/cache"
	contextx "gitlab.com/camlcase-dev/indexter/internal/context"
)

// Cache is a cache and worker for FA2 permissions
type Cache interface {
	Get(ctx context.Context, strs ...string) bool
	Set(permission bool, strs ...string)
}

type cachex struct {
	expiration     time.Duration
	purgeFrequency time.Duration
	updateInterval time.Duration
	updateFunc     func(ctx context.Context, params ...string) (bool, error)
	logger         *logrus.Logger
	cache          *gocache.Cache
}

type permissionItem struct {
	lastQueried time.Time
	allowed     bool
}

// New returns a new permission Cache
func New(logger *logrus.Logger, expiration time.Duration, purgeFrequency time.Duration, updateInterval time.Duration, updateFunc func(ctx context.Context, params ...string) (bool, error)) Cache {
	c := &cachex{
		expiration:     expiration,
		purgeFrequency: purgeFrequency,
		updateInterval: updateInterval,
		updateFunc:     updateFunc,
		logger:         logger,
		cache:          gocache.New(gocache.NoExpiration, -1), // Manually purging and expiring these values based off custom logic
	}
	c.startWorkers()
	return c
}

func (c *cachex) startWorkers() {
	ctx := context.Background()
	reqID := fmt.Sprintf("%s-PERMISSION_WORKER", uuid.New().String())
	ctx = context.WithValue(ctx, contextx.UUIDKEY, reqID)

	go func() {
		c.startPurgeWorker(ctx)
		c.startUpdateWorker(ctx)
	}()
}

func (c *cachex) startPurgeWorker(ctx context.Context) {
	ticker := time.NewTicker(c.purgeFrequency)
	for range ticker.C {
		if err := c.purge(); err != nil {
			c.logger.WithFields(logrus.Fields{
				"id":  contextx.RequestID(ctx),
				"msg": err.Error(),
			}).Error("Failed to purge balance cache.")
		}
	}
}

func (c *cachex) purge() error {
	for key, item := range c.cache.Items() {
		permissionItem, ok := item.Object.(permissionItem)
		if !ok {
			return errors.New("failed to cast cache item into permissionItem")
		}

		// if the last queried time plus the expiration interval is less than now, expired
		if permissionItem.lastQueried.Add(c.expiration).Before(time.Now()) {
			c.cache.Delete(key)
		}
	}
	return nil
}

func (c *cachex) startUpdateWorker(ctx context.Context) {
	ticker := time.NewTicker(c.updateInterval)
	for range ticker.C {
		if err := c.update(ctx); err != nil {
			c.logger.WithFields(logrus.Fields{
				"id":  contextx.RequestID(ctx),
				"msg": err.Error(),
			}).Error("Failed to update permission cache.")
		}
	}
}

func (c *cachex) update(ctx context.Context) error {
	for key, item := range c.cache.Items() {
		permissionItem, ok := item.Object.(permissionItem)
		if !ok {
			return errors.New("failed to cast cache item into permissionItem")
		}

		allowed, err := c.updateFunc(ctx, cache.FromKey(key)...)
		if err != nil {
			return err
		}

		permissionItem.allowed = allowed
		c.cache.Set(key, permissionItem, gocache.NoExpiration)
	}
	return nil
}

// Set sets a permission to a permission cache where strs are a list of strings converted to a tuple as the key
func (c *cachex) Set(permission bool, strs ...string) {
	permissionItem := permissionItem{
		lastQueried: time.Now(),
		allowed:     permission,
	}
	c.cache.Set(cache.ToKey(strs...), permissionItem, gocache.NoExpiration)
}

// Get gets a permission from a permission cache where strs are a list of strings converted to a tuple as the key
func (c *cachex) Get(ctx context.Context, strs ...string) bool {
	val, ok := c.cache.Get(cache.ToKey(strs...))
	if !ok {
		return c.notOK(ctx, strs...)
	}

	permission, ok := val.(permissionItem)
	if !ok {
		return c.notOK(ctx, strs...)
	}

	permission.lastQueried = time.Now()
	c.cache.Set(cache.ToKey(strs...), permission, gocache.NoExpiration)

	return permission.allowed
}

func (c *cachex) notOK(ctx context.Context, strs ...string) bool {
	permission, err := c.updateFunc(ctx, strs...)
	if err != nil {
		return false
	}

	c.Set(permission, strs...)
	return permission
}
