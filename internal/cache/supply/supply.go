package supply

import (
	"context"
	"fmt"
	"time"

	"github.com/google/uuid"
	gocache "github.com/patrickmn/go-cache"
	"github.com/sirupsen/logrus"
	"gitlab.com/camlcase-dev/indexter/internal/cache"
	contextx "gitlab.com/camlcase-dev/indexter/internal/context"
)

// Cache is a cache and worker for FA1.2 and FA2 supplies
type Cache interface {
	Get(ctx context.Context, strs ...string) string
	Set(supply string, strs ...string)
}

type cachex struct {
	updateInterval time.Duration
	updateFunc     func(ctx context.Context, params ...string) (string, error)
	logger         *logrus.Logger
	cache          *gocache.Cache
}

// New returns a new supply Cache
func New(logger *logrus.Logger, updateInterval time.Duration, updateFunc func(ctx context.Context, params ...string) (string, error)) Cache {
	c := &cachex{
		updateInterval: updateInterval,
		updateFunc:     updateFunc,
		logger:         logger,
		cache:          gocache.New(gocache.NoExpiration, -1),
	}
	c.startWorkers()
	return c
}

func (c *cachex) startWorkers() {
	ctx := context.Background()
	reqID := fmt.Sprintf("%s-Supply_WORKER", uuid.New().String())
	ctx = context.WithValue(ctx, contextx.UUIDKEY, reqID)

	go func() {
		c.startUpdateWorker(ctx)
	}()
}

func (c *cachex) startUpdateWorker(ctx context.Context) {
	ticker := time.NewTicker(c.updateInterval)
	for range ticker.C {
		if err := c.update(ctx); err != nil {
			c.logger.WithFields(logrus.Fields{
				"id":  contextx.RequestID(ctx),
				"msg": err.Error(),
			}).Error("Failed to update supply cache.")
		}
	}
}

func (c *cachex) update(ctx context.Context) error {
	for key := range c.cache.Items() {
		supply, err := c.updateFunc(ctx, cache.FromKey(key)...)
		if err != nil {
			return err
		}

		c.cache.Set(key, supply, gocache.NoExpiration)
	}
	return nil
}

// Set sets a supply to a supply cache where strs are a list of strings converted to a tuple as the key
func (c *cachex) Set(supply string, strs ...string) {
	c.cache.Set(cache.ToKey(strs...), supply, gocache.NoExpiration)
}

// Get gets a supply from a supply cache where strs are a list of strings converted to a tuple as the key
func (c *cachex) Get(ctx context.Context, strs ...string) string {
	val, ok := c.cache.Get(cache.ToKey(strs...))
	if !ok {
		return c.notOK(ctx, strs...)
	}

	supply, ok := val.(string)
	if !ok {
		return c.notOK(ctx, strs...)
	}

	c.cache.Set(cache.ToKey(strs...), supply, gocache.NoExpiration)
	return supply
}

func (c *cachex) notOK(ctx context.Context, strs ...string) string {
	supply, err := c.updateFunc(ctx, strs...)
	if err != nil {
		return "0"
	}

	c.Set(supply, strs...)
	return supply
}
