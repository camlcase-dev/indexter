package supply

import (
	"context"
	"testing"
	"time"

	gocache "github.com/patrickmn/go-cache"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"gitlab.com/camlcase-dev/indexter/internal/cache"
)

func Test_update(t *testing.T) {
	ctx := context.Background()

	type input struct {
		updateFunc func(ctx context.Context, params ...string) (string, error)
		cache      map[string]interface{}
		Get        []string
	}

	type want struct {
		err         bool
		errContains string
		supply      string
	}

	cases := []struct {
		name  string
		input input
		want  want
	}{
		{
			"is successful",
			input{
				func(ctx context.Context, params ...string) (string, error) {
					return "1000000", nil
				},
				map[string]interface{}{
					"(some_contract,some_token_id,some_owner)": "500000",
				},
				[]string{"some_contract", "some_token_id", "some_owner"},
			},
			want{
				false,
				"",
				"1000000",
			},
		},
	}

	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			cg := gocache.New(-1, -1)
			for key, val := range tt.input.cache {
				cg.Set(key, val, -1)
			}

			c := cachex{
				cache:      cg,
				updateFunc: tt.input.updateFunc,
			}

			err := c.update(ctx)
			checkErr(t, tt.want.err, tt.want.errContains, err)
			supply := c.Get(ctx, tt.input.Get...)
			assert.Equal(t, tt.want.supply, supply)
		})
	}
}

func Test_Get(t *testing.T) {
	ctx := context.Background()
	logger := logrus.New()
	expiration := time.Millisecond

	type input struct {
		updateFunc func(ctx context.Context, params ...string) (string, error)
		cache      map[string]string
	}

	type want struct {
		supply string
	}

	cases := []struct {
		name  string
		input input
		want  want
	}{
		{
			"handles using updateFunc",
			input{
				func(ctx context.Context, params ...string) (string, error) {
					return "1000000", nil
				},
				nil,
			},
			want{
				"1000000",
			},
		},
		{
			"handles cache",
			input{
				func(ctx context.Context, params ...string) (string, error) {
					return "1000000", nil
				},
				map[string]string{
					"(some_contract,some_token_id)": "30000000",
				},
			},
			want{
				"30000000",
			},
		},
	}

	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			c := New(logger, expiration, tt.input.updateFunc)
			for key, val := range tt.input.cache {
				c.Set(val, cache.FromKey(key)...)
			}

			supply := c.Get(ctx, "some_contract", "some_token_id")
			assert.Equal(t, tt.want.supply, supply)
		})
	}
}

func checkErr(t *testing.T, wantErr bool, errContains string, err error) {
	if wantErr {
		assert.Error(t, err)
		if err != nil {
			assert.Contains(t, err.Error(), errContains)
		}
	} else {
		assert.Nil(t, err)
	}
}
