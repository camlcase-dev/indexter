package tokenid

import (
	"context"
	"fmt"
	"time"

	"github.com/google/uuid"
	gocache "github.com/patrickmn/go-cache"
	"github.com/sirupsen/logrus"
	"gitlab.com/camlcase-dev/indexter/internal/cache"
	contextx "gitlab.com/camlcase-dev/indexter/internal/context"
)

// Cache is a cache and worker FA2 tokenIDs
type Cache interface {
	Get(ctx context.Context, strs ...string) []string
	Set(tokenIDs []string, strs ...string)
}

type cachex struct {
	updateInterval time.Duration
	updateFunc     func(ctx context.Context, params ...string) ([]string, error)
	logger         *logrus.Logger
	cache          *gocache.Cache
}

// New returns a new tokenID Cache
func New(logger *logrus.Logger, updateInterval time.Duration, updateFunc func(ctx context.Context, params ...string) ([]string, error)) Cache {
	c := &cachex{
		updateInterval: updateInterval,
		updateFunc:     updateFunc,
		logger:         logger,
		cache:          gocache.New(gocache.NoExpiration, -1),
	}
	c.startWorkers()
	return c
}

func (c *cachex) startWorkers() {
	ctx := context.Background()
	reqID := fmt.Sprintf("%s-TOKENID_WORKER", uuid.New().String())
	ctx = context.WithValue(ctx, contextx.UUIDKEY, reqID)

	go func() {
		c.startUpdateWorker(ctx)
	}()
}

func (c *cachex) startUpdateWorker(ctx context.Context) {
	ticker := time.NewTicker(c.updateInterval)
	for range ticker.C {
		if err := c.update(ctx); err != nil {
			c.logger.WithFields(logrus.Fields{
				"id":  contextx.RequestID(ctx),
				"msg": err.Error(),
			}).Error("Failed to update tokenID cache.")
		}
	}
}

func (c *cachex) update(ctx context.Context) error {
	for key := range c.cache.Items() {
		tokenIDs, err := c.updateFunc(ctx, cache.FromKey(key)...)
		if err != nil {
			return err
		}

		c.cache.Set(key, tokenIDs, gocache.NoExpiration)
	}
	return nil
}

// Set sets tokenIDs to a tokenID cache where strs are a list of strings converted to a tuple as the key
func (c *cachex) Set(tokenIDs []string, strs ...string) {
	c.cache.Set(cache.ToKey(strs...), tokenIDs, gocache.NoExpiration)
}

// Get gets tokenIDs from a tokenID cache where strs are a list of strings converted to a tuple as the key
func (c *cachex) Get(ctx context.Context, strs ...string) []string {
	val, ok := c.cache.Get(cache.ToKey(strs...))
	if !ok {
		return c.notOK(ctx, strs...)
	}

	tokenIDs, ok := val.([]string)
	if !ok {
		return c.notOK(ctx, strs...)
	}

	c.cache.Set(cache.ToKey(strs...), tokenIDs, gocache.NoExpiration)
	return tokenIDs
}

func (c *cachex) notOK(ctx context.Context, strs ...string) []string {
	supply, err := c.updateFunc(ctx, strs...)
	if err != nil {
		return []string{}
	}

	c.Set(supply, strs...)
	return supply
}
