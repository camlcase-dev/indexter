package tokenid

import (
	"context"
	"testing"
	"time"

	gocache "github.com/patrickmn/go-cache"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"gitlab.com/camlcase-dev/indexter/internal/cache"
)

func Test_update(t *testing.T) {
	ctx := context.Background()

	type input struct {
		updateFunc func(ctx context.Context, params ...string) ([]string, error)
		cache      map[string]interface{}
		Get        []string
	}

	type want struct {
		err         bool
		errContains string
		tokenIDs    []string
	}

	cases := []struct {
		name  string
		input input
		want  want
	}{
		{
			"is successful",
			input{
				func(ctx context.Context, params ...string) ([]string, error) {
					return []string{"1", "2"}, nil
				},
				map[string]interface{}{
					"(some_contract)": []string{"1"},
				},
				[]string{"some_contract"},
			},
			want{
				false,
				"",
				[]string{"1", "2"},
			},
		},
	}

	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			cg := gocache.New(-1, -1)
			for key, val := range tt.input.cache {
				cg.Set(key, val, -1)
			}

			c := cachex{
				cache:      cg,
				updateFunc: tt.input.updateFunc,
			}

			err := c.update(ctx)
			checkErr(t, tt.want.err, tt.want.errContains, err)
			tokenIDs := c.Get(ctx, tt.input.Get...)
			assert.Equal(t, tt.want.tokenIDs, tokenIDs)
		})
	}
}

func Test_Get(t *testing.T) {
	ctx := context.Background()
	logger := logrus.New()
	expiration := time.Millisecond

	type input struct {
		updateFunc func(ctx context.Context, params ...string) ([]string, error)
		cache      map[string][]string
	}

	type want struct {
		tokenIDs []string
	}

	cases := []struct {
		name  string
		input input
		want  want
	}{
		{
			"handles using updateFunc",
			input{
				func(ctx context.Context, params ...string) ([]string, error) {
					return []string{"1"}, nil
				},
				nil,
			},
			want{
				[]string{"1"},
			},
		},
		{
			"handles cache",
			input{
				func(ctx context.Context, params ...string) ([]string, error) {
					return []string{"1"}, nil
				},
				map[string][]string{
					"(some_contract)": {"1", "2"},
				},
			},
			want{
				[]string{"1", "2"},
			},
		},
	}

	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			c := New(logger, expiration, tt.input.updateFunc)
			for key, val := range tt.input.cache {
				c.Set(val, cache.FromKey(key)...)
			}

			tokenIDs := c.Get(ctx, "some_contract")
			assert.Equal(t, tt.want.tokenIDs, tokenIDs)
		})
	}
}

func checkErr(t *testing.T, wantErr bool, errContains string, err error) {
	if wantErr {
		assert.Error(t, err)
		if err != nil {
			assert.Contains(t, err.Error(), errContains)
		}
	} else {
		assert.Nil(t, err)
	}
}
