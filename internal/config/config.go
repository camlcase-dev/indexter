package config

import (
	"io/ioutil"
	"strings"
	"time"

	"github.com/go-playground/validator/v10"
	"github.com/pkg/errors"
	"gopkg.in/yaml.v2"
)

// Config -
type Config struct {
	Networks        []Network     `yaml:"networks" validate:"required"`
	CacheExpiration time.Duration `yaml:"cache_expiration"`
	CachePurge      time.Duration `yaml:"cache_purge"`
	WorkerInterval  time.Duration `yaml:"worker_interval"`
	FA2Host         string        `yaml:"fa2_host"`
}

// Network configuration
type Network struct {
	Name                     string `yaml:"name" validate:"required"`
	ContractViewAddress      string `yaml:"contract_view_address" validate:"required"`
	TransactionSourceAddress string `yaml:"transaction_source_address" validate:"required"`
	TezosRPCAddress          string `yaml:"rpc_address" validate:"required"`
}

// New loads enviroment variables into a Config struct
func New(file string) (Config, error) {
	config := Config{}
	data, err := ioutil.ReadFile(file)
	if err != nil {
		return config, errors.Wrap(err, "failed to read configuration file")
	}

	err = yaml.Unmarshal(data, &config)
	if err != nil {
		return config, errors.Wrap(err, "failed to parse configuration file")
	}

	err = validator.New().Struct(&config)
	if err != nil {
		return config, errors.Wrap(err, "configuration validation failed")
	}

	if config.CacheExpiration == 0 {
		config.CacheExpiration = time.Minute * 10
	}

	if config.CachePurge == 0 {
		config.CachePurge = time.Minute * 11
	}

	if config.WorkerInterval == 0 {
		config.WorkerInterval = time.Second * 30
	}

	for i := range config.Networks {
		config.Networks[i].Name = strings.ToLower(config.Networks[i].Name)
	}

	if config.FA2Host == "" {
		config.FA2Host = "http://localhost:3000"
	}

	return config, nil
}
