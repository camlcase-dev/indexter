package context

import "context"

// Key is a key to a context value
type Key string

const (
	// UUIDKEY is the context value key to a UUID string
	UUIDKEY Key = "UUID"

	// FA12KEY is the context value key to a FA1 string
	FA12KEY Key = "FA12"

	// FA2KEY is the context value key to a FA2 string
	FA2KEY Key = "FA2"

	// NETWORKKEY is the context value key to a NETWORK string
	NETWORKKEY Key = "NETWORK"
)

// RequestID gets the request ID from context.
func RequestID(ctx context.Context) string {
	reqID, ok := ctx.Value(UUIDKEY).(string)
	if !ok {
		return "unknown"
	}

	return reqID
}

// NetworkID gets the network from context.
func NetworkID(ctx context.Context) string {
	networkID, ok := ctx.Value(NETWORKKEY).(string)
	if !ok {
		return "unknown"
	}

	return networkID
}
