package currency

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"

	"github.com/pkg/errors"
)

const currencyProdDir = "./currencies/prod"
const currencyStagingDir = "./currencies/staging"
const currencyDevDir = "./currencies/dev"

// List is a list of Currency
type List []Currency

// Currency is a currency item in a CurrencyList
type Currency struct {
	Currency       string `json:"currency"`
	Image          string `json:"image"`
	TokenContract  string `json:"token-contract"`
	DexterContract string `json:"dexter-contract"`
	Decimals       int    `json:"decimals"`
	BigMapID       int    `json:"big-map-id"`
}

// Get returns a list of currencies
func Get(env string) (List, error) {
	var files []string
	err := filepath.Walk(currencyDir(env), func(path string, info os.FileInfo, err error) error {
		if !strings.EqualFold(path, currencyDir(env)) {
			files = append(files, path)
		}
		return nil
	})
	if err != nil {
		return nil, errors.Wrap(err, "failed to get list of currencies")
	}

	var list List
	for _, file := range files {
		data, err := ioutil.ReadFile(file)
		if err != nil {
			return nil, errors.Wrap(err, "failed to get list of currencies")
		}

		var currency Currency
		err = json.Unmarshal(data, &currency)
		if err != nil {
			return nil, errors.Wrap(err, "failed to get list of currencies: failed to parse static currency data")
		}

		list = append(list, currency)
	}

	return list, nil
}

func currencyDir(env string) string {
	if strings.ToLower(env) == "prod" {
		return currencyProdDir
	} else if strings.ToLower(env) == "staging" {
		return currencyStagingDir
	}

	return currencyDevDir
}
