package errors

import (
	"context"
	"fmt"
	"net/http"

	"github.com/go-resty/resty/v2"
	contextx "gitlab.com/camlcase-dev/indexter/internal/context"
)

// NetworkError is an error type returned as a response to a failed request
type NetworkError struct {
	StatusCode int    `json:"status_code"`
	UUID       string `json:"uuid"`
	Err        string `json:"error"`
}

// Satisfies the error interface
func (e NetworkError) Error() string {
	return fmt.Sprintf("failed to complete request (%s), status (%d): %s", e.UUID, e.StatusCode, e.Err)
}

// FromResponse creates a network error from a resty response
func FromResponse(ctx context.Context, resp *resty.Response, err string) *NetworkError {
	uuid := ctx.Value(contextx.UUIDKEY).(string)
	if resp != nil {
		return &NetworkError{StatusCode: resp.StatusCode(), UUID: uuid, Err: err}
	}

	return &NetworkError{StatusCode: http.StatusInternalServerError, UUID: uuid, Err: err}
}

// NewNetworkError returns a new network error with a status
// It assumes the UUID key in context is a string
func NewNetworkError(ctx context.Context, statusCode int, err string) *NetworkError {
	uuid := ctx.Value(contextx.UUIDKEY).(string)
	networkErr := &NetworkError{StatusCode: http.StatusInternalServerError, UUID: uuid, Err: err}
	return networkErr
}
