package currency

import (
	"encoding/json"
	"net/http"

	"github.com/sirupsen/logrus"
	"gitlab.com/camlcase-dev/indexter/internal/currency"
	errorsx "gitlab.com/camlcase-dev/indexter/internal/server/errors"
	"gitlab.com/camlcase-dev/indexter/internal/server/logging"
)

// Routes has all the Fa1.2 routes
type Routes struct {
	logger *logrus.Logger
}

// New returns new routes
func New(logger *logrus.Logger) *Routes {
	return &Routes{
		logger: logger,
	}
}

// Currencies retrieves a list of currencies
func (rts *Routes) Currencies(w http.ResponseWriter, r *http.Request) {
	var env string
	keys, ok := r.URL.Query()["env"]
	if !ok || len(keys) == 0 {
		env = "prod"
	} else {
		env = keys[0]
	}

	currencies, err := currency.Get(env)
	if err != nil {
		errorsx.HandleError(rts.logger, w, r, err)
		return
	}

	w.WriteHeader(http.StatusOK)
	encodeError := json.NewEncoder(w).Encode(currencies)
	if encodeError != nil {
		logging.LogFailureToEncode(rts.logger, w, r, http.StatusInternalServerError, encodeError.Error())
	}

	logging.LogSuccess(rts.logger, r)
}
