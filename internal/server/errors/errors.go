package errors

import (
	"encoding/json"
	"net/http"

	"github.com/sirupsen/logrus"
	errorsx "gitlab.com/camlcase-dev/indexter/internal/errors"
	"gitlab.com/camlcase-dev/indexter/internal/server/logging"
)

// HandleError -
func HandleError(logger *logrus.Logger, w http.ResponseWriter, r *http.Request, err error) {
	httpErr, ok := err.(*errorsx.NetworkError)
	if !ok {
		httpErr = errorsx.NewNetworkError(r.Context(), http.StatusInternalServerError, err.Error())
	}

	w.WriteHeader(http.StatusInternalServerError)
	encodeError := json.NewEncoder(w).Encode(httpErr)
	if encodeError != nil {
		logging.LogFailureToEncode(logger, w, r, http.StatusInternalServerError, err.Error())
	}

	logging.LogFailure(logger, w, r, http.StatusInternalServerError, err.Error())
}
