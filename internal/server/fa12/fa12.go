package fa12

import (
	"encoding/json"
	"errors"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	fa12 "gitlab.com/camlcase-dev/indexter/internal/cache/fa1.2"
	contextx "gitlab.com/camlcase-dev/indexter/internal/context"
	errorsx "gitlab.com/camlcase-dev/indexter/internal/errors"
	errorss "gitlab.com/camlcase-dev/indexter/internal/server/errors"
	"gitlab.com/camlcase-dev/indexter/internal/server/logging"
)

// Routes has all the Fa1.2 routes
type Routes struct {
	logger *logrus.Logger
}

// New returns new routes
func New(logger *logrus.Logger) *Routes {
	return &Routes{
		logger: logger,
	}
}

// Balance gets an FA1.2 balance from the cache
func (rts *Routes) Balance(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	contract := vars["contract"]
	owner := vars["owner"]
	fa12, ok := r.Context().Value(contextx.FA12KEY).(fa12.FA12)
	if !ok {
		errorss.HandleError(rts.logger, w, r, errors.New("failed to get fa1.2 driver"))
		return
	}

	balance := fa12.GetBalanceWithCache(r.Context(), contract, owner)
	w.WriteHeader(http.StatusOK)
	encodeError := json.NewEncoder(w).Encode(balance)
	if encodeError != nil {
		logging.LogFailureToEncode(rts.logger, w, r, http.StatusInternalServerError, encodeError.Error())
	}

	logging.LogSuccess(rts.logger, r)
}

// BalanceNow gets an FA1.2 balance
func (rts *Routes) BalanceNow(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	contract := vars["contract"]
	owner := vars["owner"]
	fa12, ok := r.Context().Value(contextx.FA12KEY).(fa12.FA12)
	if !ok {
		errorss.HandleError(rts.logger, w, r, errors.New("failed to get fa1.2 driver"))
		return
	}

	balance, err := fa12.GetBalance(r.Context(), contract, owner)
	if err != nil {
		w.WriteHeader(http.StatusOK)
		encodeError := json.NewEncoder(w).Encode("0")
		if encodeError != nil {
			logging.LogFailureToEncode(rts.logger, w, r, http.StatusInternalServerError, encodeError.Error())
		}

		logging.LogFailure(rts.logger, w, r, http.StatusInternalServerError, err.Error())
		return
	}

	w.WriteHeader(http.StatusOK)
	encodeError := json.NewEncoder(w).Encode(balance)
	if encodeError != nil {
		logging.LogFailureToEncode(rts.logger, w, r, http.StatusInternalServerError, encodeError.Error())
	}

	logging.LogSuccess(rts.logger, r)
}

// Supply gets an FA1.2 supply from the cache
func (rts *Routes) Supply(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	contract := vars["contract"]
	fa12, ok := r.Context().Value(contextx.FA12KEY).(fa12.FA12)
	if !ok {
		errorss.HandleError(rts.logger, w, r, errors.New("failed to get fa1.2 driver"))
		return
	}

	supply := fa12.GetSupplyWithCache(r.Context(), contract)
	w.WriteHeader(http.StatusOK)
	encodeError := json.NewEncoder(w).Encode(supply)
	if encodeError != nil {
		logging.LogFailureToEncode(rts.logger, w, r, http.StatusInternalServerError, encodeError.Error())
	}

	logging.LogSuccess(rts.logger, r)
}

// SupplyNow gets an FA1.2 supply
func (rts *Routes) SupplyNow(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	contract := vars["contract"]
	fa12, ok := r.Context().Value(contextx.FA12KEY).(fa12.FA12)
	if !ok {
		errorss.HandleError(rts.logger, w, r, errors.New("failed to get fa1.2 driver"))
		return
	}

	supply, err := fa12.GetSupply(r.Context(), contract)
	if err != nil {
		httpErr, _ := err.(*errorsx.NetworkError)
		w.WriteHeader(httpErr.StatusCode)
		encodeError := json.NewEncoder(w).Encode(httpErr)
		if encodeError != nil {
			logging.LogFailureToEncode(rts.logger, w, r, http.StatusInternalServerError, encodeError.Error())
		}

		logging.LogFailure(rts.logger, w, r, http.StatusInternalServerError, err.Error())
		return
	}

	w.WriteHeader(http.StatusOK)
	encodeError := json.NewEncoder(w).Encode(supply)
	if encodeError != nil {
		logging.LogFailureToEncode(rts.logger, w, r, http.StatusInternalServerError, encodeError.Error())
	}

	logging.LogSuccess(rts.logger, r)
}

// Allowance gets an FA1.2 allowance with the cache
func (rts *Routes) Allowance(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	contract := vars["contract"]
	owner := vars["owner"]
	spender := vars["spender"]
	fa12, ok := r.Context().Value(contextx.FA12KEY).(fa12.FA12)
	if !ok {
		errorss.HandleError(rts.logger, w, r, errors.New("failed to get fa1.2 driver"))
		return
	}

	allowance := fa12.GetAllowanceWithCache(r.Context(), contract, owner, spender)
	w.WriteHeader(http.StatusOK)
	encodeError := json.NewEncoder(w).Encode(allowance)
	if encodeError != nil {
		logging.LogFailureToEncode(rts.logger, w, r, http.StatusInternalServerError, encodeError.Error())
	}

	logging.LogSuccess(rts.logger, r)
}

// AllowanceNow gets an FA1.2 allowance without the cache
func (rts *Routes) AllowanceNow(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	contract := vars["contract"]
	owner := vars["owner"]
	spender := vars["spender"]
	fa12, ok := r.Context().Value(contextx.FA12KEY).(fa12.FA12)
	if !ok {
		errorss.HandleError(rts.logger, w, r, errors.New("failed to get fa1.2 driver"))
		return
	}

	allowance, err := fa12.GetAllowance(r.Context(), contract, owner, spender)
	if err != nil {
		//	httpErr, _ := err.(*errorsx.NetworkError)
		w.WriteHeader(http.StatusOK)
		encodeError := json.NewEncoder(w).Encode("0")
		if encodeError != nil {
			logging.LogFailureToEncode(rts.logger, w, r, http.StatusInternalServerError, encodeError.Error())
		}

		logging.LogFailure(rts.logger, w, r, http.StatusInternalServerError, err.Error())
		return
	}

	w.WriteHeader(http.StatusOK)
	encodeError := json.NewEncoder(w).Encode(allowance)
	if encodeError != nil {
		logging.LogFailureToEncode(rts.logger, w, r, http.StatusInternalServerError, encodeError.Error())
	}

	logging.LogSuccess(rts.logger, r)
}
