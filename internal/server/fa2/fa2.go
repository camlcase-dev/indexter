package fa2

import (
	"encoding/json"
	"errors"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"gitlab.com/camlcase-dev/indexter/internal/cache/fa2"
	contextx "gitlab.com/camlcase-dev/indexter/internal/context"
	errorsx "gitlab.com/camlcase-dev/indexter/internal/server/errors"
	"gitlab.com/camlcase-dev/indexter/internal/server/logging"
)

// Routes has all the Fa1.2 routes
type Routes struct {
	logger *logrus.Logger
}

// New returns new routes
func New(logger *logrus.Logger) *Routes {
	return &Routes{
		logger: logger,
	}
}

// Balance gets an FA2 balance from the cache
func (rts *Routes) Balance(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	contract := vars["contract"]
	owner := vars["owner"]
	tokenID := vars["tokenID"]
	fa2, ok := r.Context().Value(contextx.FA2KEY).(fa2.FA2)
	if !ok {
		errorsx.HandleError(rts.logger, w, r, errors.New("failed to get fa2 driver"))
		return
	}

	balance := fa2.GetBalanceWithCache(r.Context(), contract, tokenID, owner)
	w.WriteHeader(http.StatusOK)
	encodeError := json.NewEncoder(w).Encode(balance)
	if encodeError != nil {
		logging.LogFailureToEncode(rts.logger, w, r, http.StatusInternalServerError, encodeError.Error())
	}

	logging.LogSuccess(rts.logger, r)
}

// BalanceNow gets an FA2 balance
func (rts *Routes) BalanceNow(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	contract := vars["contract"]
	owner := vars["owner"]
	tokenID := vars["tokenID"]
	fa2, ok := r.Context().Value(contextx.FA2KEY).(fa2.FA2)
	if !ok {
		errorsx.HandleError(rts.logger, w, r, errors.New("failed to get fa2 driver"))
		return
	}

	balance, err := fa2.GetBalance(r.Context(), contract, tokenID, owner)
	if err != nil {
		errorsx.HandleError(rts.logger, w, r, err)
		return
	}

	w.WriteHeader(http.StatusOK)
	encodeError := json.NewEncoder(w).Encode(balance)
	if encodeError != nil {
		logging.LogFailureToEncode(rts.logger, w, r, http.StatusInternalServerError, encodeError.Error())
	}

	logging.LogSuccess(rts.logger, r)
}

// Supply gets an FA2 supply from the cache
func (rts *Routes) Supply(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	contract := vars["contract"]
	tokenID := vars["tokenID"]
	fa2, ok := r.Context().Value(contextx.FA2KEY).(fa2.FA2)
	if !ok {
		errorsx.HandleError(rts.logger, w, r, errors.New("failed to get fa2 driver"))
		return
	}

	supply := fa2.GetSupplyWithCache(r.Context(), contract, tokenID)
	w.WriteHeader(http.StatusOK)
	encodeError := json.NewEncoder(w).Encode(supply)
	if encodeError != nil {
		logging.LogFailureToEncode(rts.logger, w, r, http.StatusInternalServerError, encodeError.Error())
	}

	logging.LogSuccess(rts.logger, r)
}

// SupplyNow gets an FA2 supply
func (rts *Routes) SupplyNow(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	contract := vars["contract"]
	tokenID := vars["tokenID"]
	fa2, ok := r.Context().Value(contextx.FA2KEY).(fa2.FA2)
	if !ok {
		errorsx.HandleError(rts.logger, w, r, errors.New("failed to get fa2 driver"))
		return
	}

	supply, err := fa2.GetSupply(r.Context(), contract, tokenID)
	if err != nil {
		errorsx.HandleError(rts.logger, w, r, err)
		return
	}

	w.WriteHeader(http.StatusOK)
	encodeError := json.NewEncoder(w).Encode(supply)
	if encodeError != nil {
		logging.LogFailureToEncode(rts.logger, w, r, http.StatusInternalServerError, encodeError.Error())
	}

	logging.LogSuccess(rts.logger, r)
}

// Tokens gets the FA2 tokens from the cache
func (rts *Routes) Tokens(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	contract := vars["contract"]
	fa2, ok := r.Context().Value(contextx.FA2KEY).(fa2.FA2)
	if !ok {
		errorsx.HandleError(rts.logger, w, r, errors.New("failed to get fa2 driver"))
		return
	}

	tokens := fa2.GetTokensWithCache(r.Context(), contract)
	w.WriteHeader(http.StatusOK)
	encodeError := json.NewEncoder(w).Encode(tokens)
	if encodeError != nil {
		logging.LogFailureToEncode(rts.logger, w, r, http.StatusInternalServerError, encodeError.Error())
	}

	logging.LogSuccess(rts.logger, r)
}

// TokensNow gets FA2 tokens
func (rts *Routes) TokensNow(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	contract := vars["contract"]
	fa2, ok := r.Context().Value(contextx.FA2KEY).(fa2.FA2)
	if !ok {
		errorsx.HandleError(rts.logger, w, r, errors.New("failed to get fa2 driver"))
		return
	}

	tokens, err := fa2.GetTokens(r.Context(), contract)
	if err != nil {
		errorsx.HandleError(rts.logger, w, r, err)
		return
	}

	w.WriteHeader(http.StatusOK)
	encodeError := json.NewEncoder(w).Encode(tokens)
	if encodeError != nil {
		logging.LogFailureToEncode(rts.logger, w, r, http.StatusInternalServerError, encodeError.Error())
	}

	logging.LogSuccess(rts.logger, r)
}

// Permission gets an FA2 permission with cache
func (rts *Routes) Permission(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	contract := vars["contract"]
	owner := vars["owner"]
	spender := vars["spender"]
	tokenID := vars["tokenID"]
	fa2, ok := r.Context().Value(contextx.FA2KEY).(fa2.FA2)
	if !ok {
		errorsx.HandleError(rts.logger, w, r, errors.New("failed to get fa2 driver"))
		return
	}

	permission := fa2.GetPermissionWithCache(r.Context(), contract, tokenID, owner, spender)
	w.WriteHeader(http.StatusOK)
	encodeError := json.NewEncoder(w).Encode(permission)
	if encodeError != nil {
		logging.LogFailureToEncode(rts.logger, w, r, http.StatusInternalServerError, encodeError.Error())
	}

	logging.LogSuccess(rts.logger, r)
}

// PermissionNow gets an FA2 permission now
func (rts *Routes) PermissionNow(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	contract := vars["contract"]
	owner := vars["owner"]
	spender := vars["spender"]
	tokenID := vars["tokenID"]
	fa2, ok := r.Context().Value(contextx.FA2KEY).(fa2.FA2)
	if !ok {
		errorsx.HandleError(rts.logger, w, r, errors.New("failed to get fa2 driver"))
		return
	}

	permission, err := fa2.GetPermission(r.Context(), contract, tokenID, owner, spender)
	if err != nil {
		errorsx.HandleError(rts.logger, w, r, err)
		return
	}

	w.WriteHeader(http.StatusOK)
	encodeError := json.NewEncoder(w).Encode(permission)
	if encodeError != nil {
		logging.LogFailureToEncode(rts.logger, w, r, http.StatusInternalServerError, encodeError.Error())
	}

	logging.LogSuccess(rts.logger, r)
}
