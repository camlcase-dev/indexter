package logging

import (
	"net/http"

	"github.com/sirupsen/logrus"
	contextx "gitlab.com/camlcase-dev/indexter/internal/context"
)

// LogSuccess logs a successful request based off the network and request ID's
func LogSuccess(logger *logrus.Logger, r *http.Request) {
	reqID := contextx.RequestID(r.Context())
	network := contextx.NetworkID(r.Context())
	logrus.WithFields(logrus.Fields{
		"request_id":  reqID,
		"network":     network,
		"status_code": http.StatusOK,
		"path":        r.URL.Path,
	}).Info("Completed Request.")
}

// LogFailureToEncode logs an encoding failure based off the network and request ID's
func LogFailureToEncode(logger *logrus.Logger, w http.ResponseWriter, r *http.Request, statusCode int, err string) {
	reqID := contextx.RequestID(r.Context())
	network := contextx.NetworkID(r.Context())
	logger.WithFields(logrus.Fields{
		"request_id":  reqID,
		"network":     network,
		"status_code": statusCode,
		"path":        r.URL.Path,
		"message":     err,
	}).Error("Failed to encode body to response.")
}

// LogFailure logs a failed request based off the network and request ID's
func LogFailure(logger *logrus.Logger, w http.ResponseWriter, r *http.Request, statusCode int, err string) {
	reqID := contextx.RequestID(r.Context())
	network := contextx.NetworkID(r.Context())
	logger.WithFields(logrus.Fields{
		"request_id":  reqID,
		"network":     network,
		"status_code": statusCode,
		"path":        r.URL.Path,
		"message":     err,
	}).Error("Failed to process request.")
}
