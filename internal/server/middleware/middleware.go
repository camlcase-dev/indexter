package middleware

import (
	"context"
	"errors"
	"fmt"
	"net/http"

	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
	fa12 "gitlab.com/camlcase-dev/indexter/internal/cache/fa1.2"
	"gitlab.com/camlcase-dev/indexter/internal/cache/fa2"
	contextx "gitlab.com/camlcase-dev/indexter/internal/context"
	errorsx "gitlab.com/camlcase-dev/indexter/internal/server/errors"
)

// Cache contains the FA12 and FA2 mappings
type Cache struct {
	FA12 map[string]fa12.FA12
	FA2  map[string]fa2.FA2
}

// Middleware is http middleware for request logging and tracking
type middleware struct {
	logger            *logrus.Logger
	supportedNetworks map[string]struct{}
	caches            Cache
}

// Adapter is an alias so I dont have to type so much.
type Adapter func(http.Handler) http.Handler

// Adapt takes Handler funcs and chains them to the main handler.
func Adapt(handler http.Handler, adapters ...Adapter) http.Handler {
	for i := len(adapters); i > 0; i-- {
		handler = adapters[i-1](handler)
	}
	return handler
}

// New returns a chain of adapters (middleware)
func New(logger *logrus.Logger, supportedNetworks map[string]struct{}, caches Cache) []Adapter {
	middleware := &middleware{
		logger:            logger,
		supportedNetworks: supportedNetworks,
		caches:            caches,
	}

	return []Adapter{
		middleware.contextMiddleware,
		middleware.networkMiddleware,
		middleware.headersMiddleware,
		middleware.cacheMiddleware,
	}
}

func (m *middleware) getNetwork(req *http.Request) (string, error) {
	var network string
	keys, ok := req.URL.Query()["network"]
	if !ok || len(keys) == 0 {
		network = "mainnet"
	} else {
		_, ok := m.supportedNetworks[keys[0]]
		if !ok {
			return keys[0], fmt.Errorf("unsupported network (%s)", keys[0])
		}
		network = keys[0]
	}

	return network, nil
}

func (m *middleware) contextMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		reqID := uuid.New().String()
		r = r.WithContext(context.WithValue(r.Context(), contextx.UUIDKEY, reqID))

		m.logger.WithFields(logrus.Fields{
			"request_id": reqID,
			"path":       r.URL.Path,
		}).Info("Processing Request.")

		next.ServeHTTP(w, r)
	})
}

func (m *middleware) networkMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		network, err := m.getNetwork(r)
		if err != nil {
			errorsx.HandleError(m.logger, w, r, err)
			return
		}

		r = r.WithContext(context.WithValue(r.Context(), contextx.NETWORKKEY, network))
		next.ServeHTTP(w, r)
	})
}

func (m *middleware) headersMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		next.ServeHTTP(w, r)
	})
}

func (m *middleware) cacheMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		networkID := contextx.NetworkID(r.Context())
		fa12, ok := m.caches.FA12[networkID]
		if !ok {
			errorsx.HandleError(m.logger, w, r, errors.New("failed to access network"))
			return
		}

		fa2, ok := m.caches.FA2[networkID]
		if !ok {
			errorsx.HandleError(m.logger, w, r, errors.New("failed to access network"))
			return
		}

		r = r.WithContext(context.WithValue(r.Context(), contextx.FA12KEY, fa12))
		r = r.WithContext(context.WithValue(r.Context(), contextx.FA2KEY, fa2))
		next.ServeHTTP(w, r)
	})
}
