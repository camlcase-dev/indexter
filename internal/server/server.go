package server

import (
	"net/http"

	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"gitlab.com/camlcase-dev/indexter/internal/config"
	"gitlab.com/camlcase-dev/indexter/internal/server/currency"
	"gitlab.com/camlcase-dev/indexter/internal/server/fa12"
	"gitlab.com/camlcase-dev/indexter/internal/server/fa2"
	"gitlab.com/camlcase-dev/indexter/internal/server/middleware"
)

// Server is the Indexter API
type Server struct {
	caches            middleware.Cache
	supportedNetworks map[string]struct{}
	logger            *logrus.Logger
}

// New returns a new fa12 server
func New(config config.Config, caches middleware.Cache, logger *logrus.Logger) (*Server, error) {
	supportedNetworks := make(map[string]struct{})
	for key := range caches.FA12 {
		supportedNetworks[key] = struct{}{}
	}

	for key := range caches.FA12 {
		supportedNetworks[key] = struct{}{}
	}

	return &Server{
		caches:            caches,
		supportedNetworks: supportedNetworks,
		logger:            logger,
	}, nil
}

// Start starts the server
func (s *Server) Start() error {
	router := mux.NewRouter()
	adapters := middleware.New(s.logger, s.supportedNetworks, s.caches)
	fa12Routes := fa12.New(s.logger)
	fa2Routes := fa2.New(s.logger)
	currencyRoutes := currency.New(s.logger)

	// --------------------------- fa1.2 routes ---------------------------------
	router.HandleFunc("/fa12/{contract}/balance/{owner}", middleware.Adapt(
		http.HandlerFunc(fa12Routes.Balance),
		adapters...,
	).ServeHTTP).Methods(http.MethodGet)

	router.HandleFunc("/fa12/{contract}/balance/{owner}/now", middleware.Adapt(
		http.HandlerFunc(fa12Routes.BalanceNow),
		adapters...,
	).ServeHTTP).Methods(http.MethodGet)

	router.HandleFunc("/fa12/{contract}/supply", middleware.Adapt(
		http.HandlerFunc(fa12Routes.Supply),
		adapters...,
	).ServeHTTP).Methods(http.MethodGet)

	router.HandleFunc("/fa12/{contract}/supply/now", middleware.Adapt(
		http.HandlerFunc(fa12Routes.SupplyNow),
		adapters...,
	).ServeHTTP).Methods(http.MethodGet)

	router.HandleFunc("/fa12/{contract}/allowance/{owner}/{spender}", middleware.Adapt(
		http.HandlerFunc(fa12Routes.Allowance),
		adapters...,
	).ServeHTTP).Methods(http.MethodGet)

	router.HandleFunc("/fa12/{contract}/allowance/{owner}/{spender}/now", middleware.Adapt(
		http.HandlerFunc(fa12Routes.AllowanceNow),
		adapters...,
	).ServeHTTP).Methods(http.MethodGet)

	// --------------------------- fa2 routes ---------------------------------
	router.HandleFunc("/fa2/{contract}/{tokenID}/balance/{owner}", middleware.Adapt(
		http.HandlerFunc(fa2Routes.Balance),
		adapters...,
	).ServeHTTP).Methods(http.MethodGet)

	router.HandleFunc("/fa2/{contract}/{tokenID}/balance/{owner}/now", middleware.Adapt(
		http.HandlerFunc(fa2Routes.BalanceNow),
		adapters...,
	).ServeHTTP).Methods(http.MethodGet)

	router.HandleFunc("/fa2/{contract}/{tokenID}/supply", middleware.Adapt(
		http.HandlerFunc(fa2Routes.Supply),
		adapters...,
	).ServeHTTP).Methods(http.MethodGet)

	router.HandleFunc("/fa2/{contract}/{tokenID}/supply/now", middleware.Adapt(
		http.HandlerFunc(fa2Routes.SupplyNow),
		adapters...,
	).ServeHTTP).Methods(http.MethodGet)

	router.HandleFunc("/fa2/{contract}/tokens", middleware.Adapt(
		http.HandlerFunc(fa2Routes.Tokens),
		adapters...,
	).ServeHTTP).Methods(http.MethodGet)

	router.HandleFunc("/fa2/{contract}/tokens/now", middleware.Adapt(
		http.HandlerFunc(fa2Routes.TokensNow),
		adapters...,
	).ServeHTTP).Methods(http.MethodGet)

	router.HandleFunc("/fa2/{contract}/{tokenID}/permission/{owner}/{spender}", middleware.Adapt(
		http.HandlerFunc(fa2Routes.PermissionNow),
		adapters...,
	).ServeHTTP).Methods(http.MethodGet)

	router.HandleFunc("/fa12/{contract}/{tokenID}/permission/{owner}/{spender}/now", middleware.Adapt(
		http.HandlerFunc(fa2Routes.Permission),
		adapters...,
	).ServeHTTP).Methods(http.MethodGet)

	// --------------------------- currency routes ----------------------------
	router.HandleFunc("/currencies", middleware.Adapt(
		http.HandlerFunc(currencyRoutes.Currencies),
		adapters...,
	).ServeHTTP).Methods(http.MethodGet)

	logrus.Info("Starting Indexter on Port :8080")
	return http.ListenAndServe(":8080", router)
}
