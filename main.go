package main

import (
	"flag"

	"github.com/goat-systems/go-tezos/v4/rpc"
	"github.com/sirupsen/logrus"
	fa12 "gitlab.com/camlcase-dev/indexter/internal/cache/fa1.2"
	"gitlab.com/camlcase-dev/indexter/internal/cache/fa2"
	"gitlab.com/camlcase-dev/indexter/internal/config"
	"gitlab.com/camlcase-dev/indexter/internal/server"
	"gitlab.com/camlcase-dev/indexter/internal/server/middleware"
)

func main() {
	configFlag := flag.String("config", "./indexter.yml", "Configuration for indexter.")
	flag.Parse()

	logger := logrus.New()

	config, err := config.New(*configFlag)
	if err != nil {
		logger.WithField("error", err.Error()).Fatal("Failed to load enviroment.")
	}

	caches := middleware.Cache{
		FA12: make(map[string]fa12.FA12),
		FA2:  make(map[string]fa2.FA2),
	}

	for _, network := range config.Networks {
		r, err := rpc.New(network.TezosRPCAddress)
		if err != nil {
			logger.WithField("error", err.Error()).Errorf("Failed to initialize enviroment for network [%s].", network.Name)
		} else {
			caches.FA12[network.Name] = fa12.New(logger, config.CacheExpiration, config.CachePurge, config.WorkerInterval, network.ContractViewAddress, network.TransactionSourceAddress, r)
		}

		caches.FA2[network.Name] = fa2.New(logger, config.CacheExpiration, config.CachePurge, config.WorkerInterval, config.FA2Host, network.Name)
	}

	server, err := server.New(config, caches, logger)
	if err != nil {
		logger.WithField("error", err.Error()).Error("Failed to construct a new server.")
	} else {
		err = server.Start()
		if err != nil {
			logger.WithField("error", err.Error()).Error("Failed to start indexter.")
		}
	}
}
